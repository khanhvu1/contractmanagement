﻿using ContractManagement.DataAccess;
using ContractManagement.Libs.Authencation;
using ContractManagement.Libs.Authencation.Document.Authencation;
using ContractManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Data;

namespace ContractManagement.Controllers
{
    public class HomeController : Controller
    {
        #region Index
        [FilterPermission()]
        public ActionResult Index()
        {
            return View();
        }

        #endregion
        #region Login
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            model.PassWord = Crypto.Encrypt(model.PassWord);
            var returnInfo = PermissionSqlProvider.Db.Login(model);
            if (returnInfo != null)
            {
                SystemBL.Login(returnInfo.Id, returnInfo.Email);
                return Json(new { Code = 1, ResponseMessage = "Đăng nhập thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Code = -1, ResponseMessage = "Đăng nhập không thành công" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Logout()
        {
            AccountUtil.Id = -1;
            AccountUtil.Email = "";
            Session[Constains.SESSION_PERMISSION] = null;
            return Redirect("/Home/Login?Url=");
        }
        #endregion
        #region Customer
        [FilterPermission()]
        public ActionResult GetListCustomer(CustomerModel model)
        {
            if (model.CurrentPage == 0)
            {
                model.CurrentPage = 1;
            }
            model.PageSize = 10;
            var data = SqlProvider.Db.GetListCustomer(model);



            return View("_GetListCustomer", data);
        }

        [FilterPermission()]
        public ActionResult GetCustomer(int id)
        {
            var data = SqlProvider.Db.GetCustomerById(id);
            ViewBag.ListContactPerson = SqlProvider.Db.GetListContactPersonByCustomerId(id);
            return View("_GetCustomer", data);
        }
        [FilterPermission()]
        public ActionResult ModifyCustomer(int id)
        {
            ViewBag.ListContactPerson = new List<ContactPerson>();
            var data = SqlProvider.Db.GetCustomerById(id);
            ViewBag.ListContactPerson = SqlProvider.Db.GetListContactPersonByCustomerId(id);
            ViewBag.ListIndustry = SqlProvider.Db.SelectIndustry();
            return View("_ModifyCustomer", data);
        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyCustomerInfo(Customer model)
        {
            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyCustomer(model, ControllerId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteCustomerById(int id)
        {
            var code = SqlProvider.Db.DeleteCustomerById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportListCustomer()
        {
            // Lấy dữ liệu từ stored procedure
            var data = SqlProvider.Db.ExportListCustomer();
            // Tạo một package Excel
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage())
            {
                // Tạo một worksheet
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");
                worksheet.Cells[1, 1, 1, 12].Merge = true;
                worksheet.Cells[1, 1].Value = "BÁO ĐIỆN TỬ VTC NEWS";
                worksheet.Cells[2, 1, 2, 12].Merge = true;
                worksheet.Cells[2, 1].Value = "PHÒNG KINH DOANH";
                worksheet.Cells[4, 1, 4, 12].Merge = true;
                worksheet.Cells[4, 1].Value = "DANH SÁCH KHÁCH HÀNG ";
                // Đặt tên cho các cột
                worksheet.Cells[6, 1, 7, 1].Merge = true;
                worksheet.Cells[6, 1].Value = "STT";

                worksheet.Cells[6, 2, 7, 2].Merge = true;
                worksheet.Cells[6, 2].Value = "TÊN KHÁCH HÀNG";

                worksheet.Cells[6, 3, 7, 3].Merge = true;
                worksheet.Cells[6, 3].Value = "NGÀNH HÀNG";

                worksheet.Cells[6, 4, 7, 4].Merge = true;
                worksheet.Cells[6, 4].Value = "ĐỊA CHỈ";

                worksheet.Cells[6, 5, 7, 5].Merge = true;
                worksheet.Cells[6, 5].Value = "MST";

                worksheet.Cells[6, 6, 7, 6].Merge = true;
                worksheet.Cells[6, 6].Value = "ĐIỆN THOẠI";

                worksheet.Cells[6, 7, 7, 7].Merge = true;
                worksheet.Cells[6, 7].Value = "EMAIL";

                worksheet.Cells[6, 8, 7, 8].Merge = true;
                worksheet.Cells[6, 8].Value = "NGÀY KN THÀNH LẬP";

                worksheet.Cells[6, 9, 6, 11].Merge = true;
                worksheet.Cells[6, 9].Value = "DOANH SỐ (VNĐ)";

                worksheet.Cells[6, 12, 7, 12].Merge = true;
                worksheet.Cells[6, 12].Value = "NGƯỜI LIÊN HỆ";

                worksheet.Cells[7, 9].Value = DateTime.Now.Year - 2;
                worksheet.Cells[7, 10].Value = DateTime.Now.Year - 1;
                worksheet.Cells[7, 11].Value = DateTime.Now.Year;

                //Điều chỉnh chữ tiêu đề thành in đậm
                using (var range = worksheet.Cells["A6:L7"])
                {
                    //range.Style.Font.Bold = true;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Color.SetColor(Color.Black);
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Top.Color.SetColor(Color.Black);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                // Thêm dữ liệu từ stored procedure vào worksheet
                //var dateColumns = new List<int> { 4 };
                for (int i = 0; i < data.Count; i++)
                {
                    worksheet.Cells[i + 8, 1].Value = i + 1;
                    worksheet.Cells[i + 8, 2].Value = data[i].CustomerName;
                    worksheet.Cells[i + 8, 3].Value = data[i].IndustryName;
                    worksheet.Cells[i + 8, 4].Value = data[i].Address;
                    worksheet.Cells[i + 8, 5].Value = data[i].TaxCode;//format theo định dạng ngày tháng năm
                    worksheet.Cells[i + 8, 6].Value = data[i].Phone;
                    worksheet.Cells[i + 8, 7].Value = data[i].Email;
                    worksheet.Cells[i + 8, 8].Value = data[i].FoundationDate;
                    worksheet.Cells[i + 8, 8].Style.Numberformat.Format = "dd/MM/yyyy";
                    // Format RevenueTwoYearsAgo, RevenueLastYear, và RevenueYear dưới dạng số và thêm dấu phân cách hàng nghìn
                    worksheet.Cells[i + 8, 9].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 9].Value = data[i].RevenueTwoYearsAgo;
                    worksheet.Cells[i + 8, 10].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 10].Value = data[i].RevenueLastYear;
                    worksheet.Cells[i + 8, 11].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 11].Value = data[i].RevenueYear;


                    if (!string.IsNullOrEmpty(data[i].ContactPersons))
                    {
                        string[] contactPersonsItems = data[i].ContactPersons.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        worksheet.Cells[i + 8, 12].Value = string.Join("\n", contactPersonsItems.Select(item => "-" + item.Trim()));
                        worksheet.Cells[i + 8, 12].Style.Numberformat.Format = "@";
                    }
                    else
                    {
                        worksheet.Cells[i + 8, 12].Value = string.Empty;
                    }


                    // Thêm border cho dòng hiện tại
                    using (var range = worksheet.Cells[i + 8, 1, i + 8, 12])
                    {
                        range.Style.Border.Bottom.Style = (i == data.Count - 1) ? ExcelBorderStyle.Thin : ExcelBorderStyle.Dashed;
                        //Kiểm tra xem có phải dòng cuối cùng không, nếu đúng thì thêm nét liền, nếu không phải thì thêm nét đứt
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    }

                    // Thêm các cột khác tương ứng với data model của bạn
                }
                worksheet.Cells.AutoFitColumns();//Tự động điều chỉnh độ rộng
                using (var range = worksheet.Cells[1, 1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column])
                //chọn từ ô đầu tiên đến ô cuối cùng của bảng excell
                {
                    range.Style.Font.Name = "Times New Roman";
                    range.Style.Font.Size = 10;
                    range.Style.WrapText = true;//Điều chỉnh cho wraptext tất cả ô trong excell
                    range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;//Căn chỉnh chữ nằm giữa ô
                }
                // Gửi file Excel về client
                byte[] fileContents = package.GetAsByteArray();
                return File(fileContents, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Danh sách khách hàng.xlsx");
            }
        }
        #endregion 
        #region ContactPerson

        [FilterPermission()]
        public ActionResult ModifyContactPerson(int id)
        {

            ContactPerson data = new ContactPerson();
            if (id != 0)
            {
                data = SqlProvider.Db.GetContactPersonById(id);
            }
            return View("_ModifyContactPerson", data);

        }

        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyContactPersonInfo(ContactPerson model)
        {
            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyContactPerson(model, ControllerId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteContactPersonInfoById(int id)
        {
            var code = SqlProvider.Db.DeleteContactPersonById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        #endregion
        #region Staff
        [FilterPermission()]
        public ActionResult GetListStaff(SearchStaff model)
        {
            if (model.Year == null)
            {
                model.Year = DateTime.Now.Year;
            }
            if (model.CurrentPage == 0)
            {
                model.CurrentPage = 1;
            }
            model.PageSize = 10;
            var data = SqlProvider.Db.GetListStaff(model);
            return View("_GetListStaff", data);
        }

        [FilterPermission()]
        public ActionResult GetStaff(int id)
        {
            var data = SqlProvider.Db.GetStaff(id);
            return View("_GetStaff", data);
        }
        [FilterPermission()]
        public ActionResult AddStaff()
        {
            ListStaff data = new ListStaff();
            ViewBag.ListStaff = SqlProvider.Db.SelectStaff();
            return View("_AddStaff", data);
        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult AddStaffInfo(ListStaff model)
        {
            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.AddStaff(model, ControllerId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ModifyKpi(int id)
        {
            var data = SqlProvider.Db.GetStaff(id);
            ViewBag.UserName = data.UserName;
            ViewBag.UserId = data.UserId;
            ViewBag.Year = data.Year;
            return View("_ModifyKpi", data);
        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyKpiInfo(KPI model)
        {
            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyKpi(model, ControllerId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteStaffById(int id)
        {
            var code = SqlProvider.Db.DeleteStaffById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportListStaff(int year)
        {
            // Lấy dữ liệu từ stored procedure
            var data = SqlProvider.Db.ExportListStaff(year);
            // Tạo một package Excel
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage())
            {
                // Tạo một worksheet
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");
                worksheet.Cells[1, 1, 1, 28].Merge = true;
                worksheet.Cells[1, 1].Value = "BÁO ĐIỆN TỬ VTC NEWS";
                worksheet.Cells[2, 1, 2, 28].Merge = true;
                worksheet.Cells[2, 1].Value = "PHÒNG KINH DOANH";
                worksheet.Cells[4, 1, 4, 28].Merge = true;
                worksheet.Cells[4, 1].Value = "THEO DÕI DOANH THU THEO NHÂN SỰ NĂM" + year.ToString();
                // Đặt tên cho các cột
                worksheet.Cells[6, 1, 7, 1].Merge = true;
                worksheet.Cells[6, 1].Value = "STT";
                worksheet.Cells[6, 2, 7, 2].Merge = true;
                worksheet.Cells[6, 2].Value = "TÊN NHÂN VIÊN";
                worksheet.Cells[6, 3, 7, 3].Merge = true;
                worksheet.Cells[6, 3].Value = "NĂM";
                worksheet.Cells[6, 4, 6, 5].Merge = true;
                worksheet.Cells[6, 4].Value = "THÁNG 1";
                worksheet.Cells[6, 6, 6, 7].Merge = true;
                worksheet.Cells[6, 6].Value = "THÁNG 2";
                worksheet.Cells[6, 8, 6, 9].Merge = true;
                worksheet.Cells[6, 8].Value = "THÁNG 3";
                worksheet.Cells[6, 10, 6, 11].Merge = true;
                worksheet.Cells[6, 10].Value = "THÁNG 4";
                worksheet.Cells[6, 12, 6, 13].Merge = true;
                worksheet.Cells[6, 12].Value = "THÁNG 5";
                worksheet.Cells[6, 14, 6, 15].Merge = true;
                worksheet.Cells[6, 14].Value = "THÁNG 6";
                worksheet.Cells[6, 16, 6, 17].Merge = true;
                worksheet.Cells[6, 16].Value = "THÁNG 7";
                worksheet.Cells[6, 18, 6, 19].Merge = true;
                worksheet.Cells[6, 18].Value = "THÁNG 8";
                worksheet.Cells[6, 20, 6, 21].Merge = true;
                worksheet.Cells[6, 20].Value = "THÁNG 9";
                worksheet.Cells[6, 22, 6, 23].Merge = true;
                worksheet.Cells[6, 22].Value = "THÁNG 10";
                worksheet.Cells[6, 24, 6, 25].Merge = true;
                worksheet.Cells[6, 24].Value = "THÁNG 11";
                worksheet.Cells[6, 26, 6, 27].Merge = true;
                worksheet.Cells[6, 26].Value = "THÁNG 12";
                worksheet.Cells[6, 28, 7, 28].Merge = true;
                worksheet.Cells[6, 28].Value = "TỔNG KPI";
                worksheet.Cells[7, 4].Value = "KPI";
                worksheet.Cells[7, 5].Value = "Doanh số";
                worksheet.Cells[7, 6].Value = "KPI";
                worksheet.Cells[7, 7].Value = "Doanh số";
                worksheet.Cells[7, 8].Value = "KPI";
                worksheet.Cells[7, 9].Value = "Doanh số";
                worksheet.Cells[7, 10].Value = "KPI";
                worksheet.Cells[7, 11].Value = "Doanh số";
                worksheet.Cells[7, 12].Value = "KPI";
                worksheet.Cells[7, 13].Value = "Doanh số";
                worksheet.Cells[7, 14].Value = "KPI";
                worksheet.Cells[7, 15].Value = "Doanh số";
                worksheet.Cells[7, 16].Value = "KPI";
                worksheet.Cells[7, 17].Value = "Doanh số";
                worksheet.Cells[7, 18].Value = "KPI";
                worksheet.Cells[7, 19].Value = "Doanh số";
                worksheet.Cells[7, 20].Value = "KPI";
                worksheet.Cells[7, 21].Value = "Doanh số";
                worksheet.Cells[7, 22].Value = "KPI";
                worksheet.Cells[7, 23].Value = "Doanh số";
                worksheet.Cells[7, 24].Value = "KPI";
                worksheet.Cells[7, 25].Value = "Doanh số";
                worksheet.Cells[7, 26].Value = "KPI";
                worksheet.Cells[7, 27].Value = "Doanh số";
                //Điều chỉnh chữ tiêu đề thành in đậm
                using (var range = worksheet.Cells["A6:AB7"])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Color.SetColor(Color.Black);
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Top.Color.SetColor(Color.Black);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                // Thêm dữ liệu từ stored procedure vào worksheet
                //var dateColumns = new List<int> { 4 };
                for (int i = 0; i < data.Count; i++)
                {
                    worksheet.Cells[i + 8, 1].Value = i + 1;
                    worksheet.Cells[i + 8, 2].Value = data[i].UserName;
                    worksheet.Cells[i + 8, 3].Value = data[i].Year;
                    worksheet.Cells[i + 8, 4].Value = data[i].Kpi1;
                    worksheet.Cells[i + 8, 4].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 5].Value = data[i].TotalRevenue1;
                    worksheet.Cells[i + 8, 5].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 6].Value = data[i].Kpi2;
                    worksheet.Cells[i + 8, 6].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 7].Value = data[i].TotalRevenue2;
                    worksheet.Cells[i + 8, 7].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 8].Value = data[i].Kpi3;
                    worksheet.Cells[i + 8, 8].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 9].Value = data[i].TotalRevenue3;
                    worksheet.Cells[i + 8, 9].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 10].Value = data[i].Kpi4;
                    worksheet.Cells[i + 8, 10].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 11].Value = data[i].TotalRevenue4;
                    worksheet.Cells[i + 8, 11].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 12].Value = data[i].Kpi5;
                    worksheet.Cells[i + 8, 12].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 13].Value = data[i].TotalRevenue5;
                    worksheet.Cells[i + 8, 13].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 14].Value = data[i].Kpi6;
                    worksheet.Cells[i + 8, 14].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 15].Value = data[i].TotalRevenue6;
                    worksheet.Cells[i + 8, 15].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 16].Value = data[i].Kpi7;
                    worksheet.Cells[i + 8, 16].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 17].Value = data[i].TotalRevenue7;
                    worksheet.Cells[i + 8, 17].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 18].Value = data[i].Kpi8;
                    worksheet.Cells[i + 8, 18].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 19].Value = data[i].TotalRevenue8;
                    worksheet.Cells[i + 8, 19].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 20].Value = data[i].Kpi9;
                    worksheet.Cells[i + 8, 20].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 21].Value = data[i].TotalRevenue9;
                    worksheet.Cells[i + 8, 21].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 22].Value = data[i].Kpi10;
                    worksheet.Cells[i + 8, 22].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 23].Value = data[i].TotalRevenue10;
                    worksheet.Cells[i + 8, 23].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 24].Value = data[i].Kpi11;
                    worksheet.Cells[i + 8, 24].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 25].Value = data[i].TotalRevenue11;
                    worksheet.Cells[i + 8, 25].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 26].Value = data[i].Kpi12;
                    worksheet.Cells[i + 8, 26].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 27].Value = data[i].TotalRevenue12;
                    worksheet.Cells[i + 8, 27].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 28].Value = data[i].TotalRevenueSum;
                    worksheet.Cells[i + 8, 28].Style.Numberformat.Format = "#,##0";
                    // Thêm border cho dòng hiện tại
                    using (var range = worksheet.Cells[i + 8, 1, i + 8, 28])
                    {
                        range.Style.Border.Bottom.Style = (i == data.Count - 1) ? ExcelBorderStyle.Thin : ExcelBorderStyle.Dashed;
                        //Kiểm tra xem có phải dòng cuối cùng không, nếu đúng thì thêm nét liền, nếu không phải thì thêm nét đứt
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    }

                    // Thêm các cột khác tương ứng với data model của bạn
                }
                worksheet.Cells.AutoFitColumns();//Tự động điều chỉnh độ rộng
                using (var range = worksheet.Cells[1, 1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column])
                //chọn từ ô đầu tiên đến ô cuối cùng của bảng excell
                {
                    range.Style.Font.Name = "Times New Roman";
                    range.Style.Font.Size = 10;
                    range.Style.WrapText = true;//Điều chỉnh cho wraptext tất cả ô trong excell
                    range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;//Căn chỉnh chữ nằm giữa ô
                }
                // Gửi file Excel về client
                byte[] fileContents = package.GetAsByteArray();
                return File(fileContents, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Nhân sự-KPI.xlsx");
            }
        }
        #endregion
        #region RevenueDebt
        public ActionResult RevenueDebt(SearchRevenueDebt model)
        {
            if (model.Year == null)
            {
                model.Year = DateTime.Now.Year;
            }
            if (model.CurrentPage == 0)
            {
                model.CurrentPage = 1;
            }
            model.PageSize = 10;
            var data = SqlProvider.Db.GetRevenueDebt(model);
            return View("_RevenueDebt", data);
        }

        [FilterPermission()]
        public ActionResult GetContract(int id, int year)
        {
            var data = SqlProvider.Db.GetContractById(id, year);
            ViewBag.listRevenue = SqlProvider.Db.GetListRevenueByRevenueDebtId(id);
            ViewBag.listDebt = SqlProvider.Db.GetListDebtByRevenueDebtId(id);
            return View("_GetContract", data);
        }
        [FilterPermission()]
        public ActionResult ModifyContract(int id, int year)
        {
            RevenueDebtModel data = new RevenueDebtModel();
            if (id != 0)
            {
                data = SqlProvider.Db.GetContractById(id, year);
            }
            ViewBag.staff = SqlProvider.Db.Selectpersonnel();
            ViewBag.ListCustomer = SqlProvider.Db.SelectCustomer();
            ViewBag.listRevenue = SqlProvider.Db.GetListRevenueByRevenueDebtId(id);
            ViewBag.listDebt = SqlProvider.Db.GetListDebtByRevenueDebtId(id);
            return View("_ModifyContract", data);
        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyContractInfo(RevenueDebt model)
        {
            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyContract(model, ControllerId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }

        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteContractById(int id)
        {
            var code = SqlProvider.Db.DeleteContractById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportRevenueDebt(int year)
        {
            DataSet ds = new DataSet();
            ds = SqlProvider.Db.ExportRevenueDebt(year);

            List<RevenueDebtModel> revenueDebt = new List<RevenueDebtModel>();
            List<RevenueModel> revenue = new List<RevenueModel>();
            List<RevenueType> revenueType = new List<RevenueType>();
            //SourceSimpleModel sourceSimpleModel = new SourceSimpleModel();
            //List<ArticleSuggestionContentModel> suggersioncontent = new List<ArticleSuggestionContentModel>();
            //ArticleFontModel fontModel = new ArticleFontModel();


            if (ds.Tables[0] != null)
            {
                revenueDebt = DbExtensions.ConvertDataTable<RevenueDebtModel>(ds.Tables[0]);
            }
            if (ds.Tables[1] != null)
            {
                revenue = DbExtensions.ConvertDataTable<RevenueModel>(ds.Tables[1]);
            }
            if (ds.Tables[2] != null)
            {
                revenueType = DbExtensions.ConvertDataTable<RevenueType>(ds.Tables[2]);
            }
            //if (ds.Tables.Count > 0)
            //{
            //    revenueDebt = DbExtensions.ConvertDataTable<RevenueDebtModel>(ds.Tables[0]);
            //    revenue = DbExtensions.ConvertDataTable<RevenueModel>(ds.Tables[1]);
            //}
            var lstIdType = revenueType.Select(c => c.Id).ToList();
            int count = 0;
            if (ds.Tables.Count > 3 && ds.Tables[3].Rows.Count > 0)
            {
                count = Convert.ToInt32(ds.Tables[3].Rows[0]["Count"]);
            }
            // Lấy dữ liệu từ stored procedure
            //var data = SqlProvider.Db.ExportRevenueDebt();
            // Tạo một package Excel
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage())
            {
                // Tạo một worksheet
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");
                worksheet.Cells[1, 1, 1, 23 + count].Merge = true;
                worksheet.Cells[1, 1].Value = "BÁO ĐIỆN TỬ VTC NEWS";
                worksheet.Cells[2, 1, 2, 23 + count].Merge = true;
                worksheet.Cells[2, 1].Value = "PHÒNG KINH DOANH";
                worksheet.Cells[4, 1, 4, 23 + count].Merge = true;
                worksheet.Cells[4, 1].Value = "THEO DÕI DOANH THU - CÔNG NỢ NĂM " + year.ToString();
                // Đặt tên cho các cột
                worksheet.Cells[6, 1, 7, 1].Merge = true;
                worksheet.Cells[6, 1].Value = "STT";
                worksheet.Cells[6, 2, 7, 2].Merge = true;
                worksheet.Cells[6, 2].Value = "TÊN KHÁCH HÀNG";
                worksheet.Cells[6, 3, 7, 3].Merge = true;
                worksheet.Cells[6, 3].Value = "SỐ HỢP ĐỒNG";
                worksheet.Cells[6, 4, 7, 4].Merge = true;
                worksheet.Cells[6, 4].Value = "NĂM";
                worksheet.Cells[6, 5, 7, 5].Merge = true;
                worksheet.Cells[6, 5].Value = "NGÀY KÝ";
                worksheet.Cells[6, 6, 7, 6].Merge = true;
                worksheet.Cells[6, 6].Value = "NGÀY HẾT HẠN";
                worksheet.Cells[6, 7, 7, 7].Merge = true;
                worksheet.Cells[6, 7].Value = "GIÁ TRỊ HỢP ĐỒNG";
                worksheet.Cells[6, 8, 7, 8].Merge = true;
                worksheet.Cells[6, 8].Value = "NGƯỜI KHAI THÁC";
                worksheet.Cells[6, 9, 6, 20].Merge = true;
                worksheet.Cells[6, 9].Value = "DOANH THU XUẤT HÓA ĐƠN (VNĐ)";
                worksheet.Cells[7, 9].Value = "Tháng 1";
                worksheet.Cells[7, 10].Value = "Tháng 2";
                worksheet.Cells[7, 11].Value = "Tháng 3";
                worksheet.Cells[7, 12].Value = "Tháng 4";
                worksheet.Cells[7, 13].Value = "Tháng 5";
                worksheet.Cells[7, 14].Value = "Tháng 6";
                worksheet.Cells[7, 15].Value = "Tháng 7";
                worksheet.Cells[7, 16].Value = "Tháng 8";
                worksheet.Cells[7, 17].Value = "Tháng 9";
                worksheet.Cells[7, 18].Value = "Tháng 10";
                worksheet.Cells[7, 19].Value = "Tháng 11";
                worksheet.Cells[7, 20].Value = "Tháng 12";
                worksheet.Cells[6, 21, 6, 23].Merge = true;
                worksheet.Cells[6, 21].Value = "CÔNG NỢ";
                worksheet.Cells[7, 21].Value = "CÔNG NỢ";
                worksheet.Cells[7, 22].Value = "ĐÃ THU";
                worksheet.Cells[7, 23].Value = "PHẢI THU";
                worksheet.Cells[6, 24, 6, 23 + count].Merge = true;
                worksheet.Cells[6, 24].Value = "PHÂN LOẠI DOANH THU";
                for (int j = 0; j < revenueType.Count; j++)
                {
                    worksheet.Cells[7, 24 + j].Value = revenueType[j].Name;

                }
                //Điều chỉnh chữ tiêu đề thành in đậm
                using (var range = worksheet.Cells[6, 1, 7, 23 + count])

                {
                    range.Style.Font.Bold = true;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Color.SetColor(Color.Black);
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Top.Color.SetColor(Color.Black);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                // Thêm dữ liệu từ stored procedure vào worksheet
                //var dateColumns = new List<int> { 4 };
                for (int i = 0; i < revenueDebt.Count; i++)
                {
                    worksheet.Cells[i + 8, 1].Value = i + 1;
                    worksheet.Cells[i + 8, 2].Value = revenueDebt[i].CustomerName;
                    worksheet.Cells[i + 8, 3].Value = revenueDebt[i].ContractNumber;
                    worksheet.Cells[i + 8, 4].Value = revenueDebt[i].Year;
                    worksheet.Cells[i + 8, 5].Value = revenueDebt[i].SigningDate;//format theo định dạng ngày tháng năm
                    worksheet.Cells[i + 8, 5].Style.Numberformat.Format = "dd/MM/yyyy";
                    worksheet.Cells[i + 8, 6].Value = revenueDebt[i].ExpiryDate;
                    worksheet.Cells[i + 8, 6].Style.Numberformat.Format = "dd/MM/yyyy";
                    worksheet.Cells[i + 8, 7].Value = revenueDebt[i].ContractValue;
                    worksheet.Cells[i + 8, 7].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 8].Value = revenueDebt[i].UserName;
                    // Format RevenueTwoYearsAgo, RevenueLastYear, và RevenueYear dưới dạng số và thêm dấu phân cách hàng nghìn
                    worksheet.Cells[i + 8, 9].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 9].Value = revenueDebt[i].Revenue1;
                    worksheet.Cells[i + 8, 10].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 10].Value = revenueDebt[i].Revenue2;
                    worksheet.Cells[i + 8, 11].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 11].Value = revenueDebt[i].Revenue3;
                    worksheet.Cells[i + 8, 12].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 12].Value = revenueDebt[i].Revenue4;
                    worksheet.Cells[i + 8, 13].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 13].Value = revenueDebt[i].Revenue5;
                    worksheet.Cells[i + 8, 14].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 14].Value = revenueDebt[i].Revenue6;
                    worksheet.Cells[i + 8, 15].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 15].Value = revenueDebt[i].Revenue7;
                    worksheet.Cells[i + 8, 16].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 16].Value = revenueDebt[i].Revenue8;
                    worksheet.Cells[i + 8, 17].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 17].Value = revenueDebt[i].Revenue9;
                    worksheet.Cells[i + 8, 18].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 18].Value = revenueDebt[i].Revenue10;
                    worksheet.Cells[i + 8, 19].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 19].Value = revenueDebt[i].Revenue11;
                    worksheet.Cells[i + 8, 20].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 20].Value = revenueDebt[i].Revenue12;
                    worksheet.Cells[i + 8, 21].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 21].Value = revenueDebt[i].DebtValue;
                    worksheet.Cells[i + 8, 22].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 22].Value = revenueDebt[i].TotalDebtCollected;
                    worksheet.Cells[i + 8, 23].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[i + 8, 23].Value = revenueDebt[i].Receivables;

                    var lstCurRevenue = revenue.Where(c => c.RevenueDebtId == revenueDebt[i].RevenueDebtId);
                    foreach (var c in lstCurRevenue)
                    {
                        worksheet.Cells[i + 8, 24 + lstIdType.IndexOf(Convert.ToInt32(c.TypeId))].Value = c.RevenueValue;
                        worksheet.Cells[i + 8, 24 + lstIdType.IndexOf(Convert.ToInt32(c.TypeId))].Style.Numberformat.Format = "#,##0";
                    }

                    //for (int k = 0; k < revenueType.Count; k++)
                    //{
                    //    if (worksheet.Cells[7, 24 + k].Value?.ToString() == revenueType[k].Name && revenue[k].RevenueDebtId == revenueDebt[i].RevenueDebtId)
                    //    {
                    //        worksheet.Cells[i + 8, 24 + k].Value = revenue[k].RevenueValue;
                    //    }
                    //}




                    // Thêm border cho dòng hiện tại
                    using (var range = worksheet.Cells[i + 8, 1, i + 8, 23 + count])
                    {
                        range.Style.Border.Bottom.Style = (i == revenueDebt.Count - 1) ? ExcelBorderStyle.Thin : ExcelBorderStyle.Dashed;
                        //Kiểm tra xem có phải dòng cuối cùng không, nếu đúng thì thêm nét liền, nếu không phải thì thêm nét đứt
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    }

                    // Thêm các cột khác tương ứng với data model của bạn
                }
                worksheet.Cells.AutoFitColumns();//Tự động điều chỉnh độ rộng
                using (var range = worksheet.Cells[1, 1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column])
                //chọn từ ô đầu tiên đến ô cuối cùng của bảng excell
                {
                    range.Style.Font.Name = "Times New Roman";
                    range.Style.Font.Size = 10;
                    range.Style.WrapText = true;//Điều chỉnh cho wraptext tất cả ô trong excell
                    range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;//Căn chỉnh chữ nằm giữa ô
                }
                // Gửi file Excel về client
                byte[] fileContents = package.GetAsByteArray();
                return File(fileContents, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Doanh thu-công nợ.xlsx");
            }
        }

        #endregion
        #region Revenue
        [FilterPermission()]
        public ActionResult ModifyRevenue(int id)
        {
            ViewBag.listRevenueType = SqlProvider.Db.SelectTypeRevenue();
            RevenueModel data = new RevenueModel();
            if (id != 0)
            {
                data = SqlProvider.Db.GetRevenueById(id);


            }
            return View("_ModifyRevenue", data);
        }

        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyRevenueInfo(Revenue model)
        {

            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyRevenue(model, ControllerId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteRevenueById(int id)
        {

            var code = SqlProvider.Db.DeleteRevenueById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        #endregion
        #region ContractTracking
        [FilterPermission()]
        public ActionResult ContractTracking(SearchContractTracking model)
        {
            if (model.Year == null)
            {
                model.Year = DateTime.Now.Year;
            }
            if (model.CurrentPage == 0)
            {
                model.CurrentPage = 1;
            }
            model.PageSize = 10;
            var data = SqlProvider.Db.ContractTracking(model);
            return View("_ContractTracking", data);
        }
        public ActionResult GetContractTracking(int id)
        {
            var data = SqlProvider.Db.GetContractTrackingById(id);
            ViewBag.listService = SqlProvider.Db.GetListServiceByContractTrackingId(id);
            return View("_GetContractTracking", data);
        }
        [FilterPermission()]
        public ActionResult ModifyContractTracking(int id)
        {
            var data = SqlProvider.Db.GetContractTrackingById(id);
            ViewBag.listService = SqlProvider.Db.GetListServiceByContractTrackingId(id);
            return View("_ModifyContractTracking", data);
        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyContractTrackingInfo(ContractTracking model)
        {
            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyContractTracking(model, ControllerId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportContractTracking(int year)
        {
            DataSet ds = new DataSet();
            ds = SqlProvider.Db.ExportContractTracking(year);

            List<ContrackTrackingModel> contractTracking = new List<ContrackTrackingModel>();
            List<ServiceModel> service = new List<ServiceModel>();
            List<SubscriptionService> serviceType = new List<SubscriptionService>();
            //SourceSimpleModel sourceSimpleModel = new SourceSimpleModel();
            //List<ArticleSuggestionContentModel> suggersioncontent = new List<ArticleSuggestionContentModel>();
            //ArticleFontModel fontModel = new ArticleFontModel();


            if (ds.Tables[0] != null)
            {
                contractTracking = DbExtensions.ConvertDataTable<ContrackTrackingModel>(ds.Tables[0]);
            }
            if (ds.Tables[1] != null)
            {
                service = DbExtensions.ConvertDataTable<ServiceModel>(ds.Tables[1]);
            }
            if (ds.Tables[2] != null)
            {
                serviceType = DbExtensions.ConvertDataTable<SubscriptionService>(ds.Tables[2]);
            }
            var lstIdType = serviceType.Select(c => c.Id).ToList();
            int count = 0;
            if (ds.Tables.Count > 3 && ds.Tables[3].Rows.Count > 0)
            {
                count = Convert.ToInt32(ds.Tables[3].Rows[0]["Count"]);
            }
            // Tạo một package Excel
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage())
            {
                // Tạo một worksheet
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");
                worksheet.Cells[1, 1, 1, 9].Merge = true;
                worksheet.Cells[1, 1].Value = "BÁO ĐIỆN TỬ VTC NEWS";
                worksheet.Cells[2, 1, 2, 9].Merge = true;
                worksheet.Cells[2, 1].Value = "PHÒNG KINH DOANH";
                worksheet.Cells[4, 1, 4, 9].Merge = true;
                worksheet.Cells[4, 1].Value = "THEO DÕI  THỰC HIỆN HỢP ĐỒNG NĂM " + year.ToString();
                // Đặt tên cho các cột
                worksheet.Cells[6, 1, 7, 1].Merge = true;
                worksheet.Cells[6, 1].Value = "STT";
                worksheet.Cells[6, 2, 7, 2].Merge = true;
                worksheet.Cells[6, 2].Value = "TÊN KHÁCH HÀNG";
                worksheet.Cells[6, 3, 7, 3].Merge = true;
                worksheet.Cells[6, 3].Value = "SỐ HỢP ĐỒNG";
                worksheet.Cells[6, 4, 7, 4].Merge = true;
                worksheet.Cells[6, 4].Value = "NĂM";
                worksheet.Cells[6, 5, 7, 5].Merge = true;
                worksheet.Cells[6, 5].Value = "NGÀY KÝ";
                worksheet.Cells[6, 6, 7, 6].Merge = true;
                worksheet.Cells[6, 6].Value = "NGÀY HẾT HẠN";
                worksheet.Cells[6, 7, 7, 7].Merge = true;
                worksheet.Cells[6, 7].Value = "GIÁ TRỊ HỢP ĐỒNG";
                worksheet.Cells[6, 8, 6, 7 + count].Merge = true;
                worksheet.Cells[6, 8].Value = "LOẠI DỊCH VỤ";
                for (int j = 0; j < serviceType.Count; j++)
                {
                    worksheet.Cells[7, 8 + j].Value = serviceType[j].Name;

                }
                //Điều chỉnh chữ tiêu đề thành in đậm
                using (var range = worksheet.Cells[6, 1, 7, 7 + count])

                {
                    range.Style.Font.Bold = true;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Color.SetColor(Color.Black);
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Top.Color.SetColor(Color.Black);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                // Thêm dữ liệu từ stored procedure vào worksheet
                //var dateColumns = new List<int> { 4 };
                for (int i = 0; i < contractTracking.Count; i++)
                {
                    worksheet.Cells[i + 8, 1].Value = i + 1;
                    worksheet.Cells[i + 8, 2].Value = contractTracking[i].CustomerName;
                    worksheet.Cells[i + 8, 3].Value = contractTracking[i].ContractNumber;
                    worksheet.Cells[i + 8, 4].Value = contractTracking[i].Year;
                    worksheet.Cells[i + 8, 5].Value = contractTracking[i].SigningDate;//format theo định dạng ngày tháng năm
                    worksheet.Cells[i + 8, 5].Style.Numberformat.Format = "dd/MM/yyyy";
                    worksheet.Cells[i + 8, 6].Value = contractTracking[i].ExpiryDate;
                    worksheet.Cells[i + 8, 6].Style.Numberformat.Format = "dd/MM/yyyy";
                    worksheet.Cells[i + 8, 7].Value = contractTracking[i].ContractValue;
                    worksheet.Cells[i + 8, 7].Style.Numberformat.Format = "#,##0";

                    // Format RevenueTwoYearsAgo, RevenueLastYear, và RevenueYear dưới dạng số và thêm dấu phân cách hàng nghìn

                    for (int k = 0; k < serviceType.Count; k++)
                    {
                        {
                            worksheet.Cells[i + 8, 8 + k].Value = "Không đăng ký";
                        }
                    }

                    var lstCurRevenue = service.Where(c => c.ContractTrackingId == contractTracking[i].ContractTrackingId);
                    foreach (var c in lstCurRevenue)
                    {
                        // Kiểm tra giá trị của ServiceStatus và đặt giá trị tương ứng vào ô
                        var cellValue = c.ServiceStatus;
                        string cellText;

                        if (cellValue == 1)
                        {
                            cellText = "Đã thực hiện";
                        }
                        else
                        {
                            cellText = "Chưa thực hiện";
                        }

                        // Đặt giá trị vào ô
                        worksheet.Cells[i + 8, 8 + lstIdType.IndexOf(Convert.ToInt32(c.TypeId))].Value = cellText;
                    }
                    // Thêm border cho dòng hiện tại
                    using (var range = worksheet.Cells[i + 8, 1, i + 8, 7 + count])
                    {
                        range.Style.Border.Bottom.Style = (i == contractTracking.Count - 1) ? ExcelBorderStyle.Thin : ExcelBorderStyle.Dashed;
                        //Kiểm tra xem có phải dòng cuối cùng không, nếu đúng thì thêm nét liền, nếu không phải thì thêm nét đứt
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    }

                    // Thêm các cột khác tương ứng với data model của bạn
                }
                worksheet.Cells.AutoFitColumns();//Tự động điều chỉnh độ rộng
                using (var range = worksheet.Cells[1, 1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column])
                //chọn từ ô đầu tiên đến ô cuối cùng của bảng excell
                {
                    range.Style.Font.Name = "Times New Roman";
                    range.Style.Font.Size = 10;
                    range.Style.WrapText = true;//Điều chỉnh cho wraptext tất cả ô trong excell
                    range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;//Căn chỉnh chữ nằm giữa ô
                }
                // Gửi file Excel về client
                byte[] fileContents = package.GetAsByteArray();
                return File(fileContents, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Trả quyền lợi.xlsx");
            }
        }
        #endregion
        #region Service
        [FilterPermission()]
        public ActionResult ModifyService(int id)
        {
            ViewBag.listServiceType = SqlProvider.Db.SelectTypeService();
            ServiceModel data = new ServiceModel();
            if (id != 0)
            {
                data = SqlProvider.Db.GetServiceById(id);


            }
            return View("_ModifyService", data);
        }

        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyServiceInfo(Service model)
        {

            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyService(model);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteServiceById(int id)
        {

            var code = SqlProvider.Db.DeleteServiceById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        #endregion
        #region Debt
        [FilterPermission()]
        public ActionResult ModifyDebt(int id)
        {
            Debt data = new Debt();
            if (id != 0)
            {
                data = SqlProvider.Db.GetDebtById(id);


            }
            return View("_ModifyDebt", data);
        }

        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyDebtInfo(Debt model)
        {

            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyDebt(model, ControllerId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteDebtById(int id)
        {

            var code = SqlProvider.Db.DeleteDebtById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        #endregion
        #region OldDebt
        [FilterPermission()]
        public ActionResult ModifyOldDebt(int id)
        {
            OldDebt data = new OldDebt();
            if (id != 0)
            {
                data = SqlProvider.Db.GetOldDebtById(id);


            }
            ViewBag.ListCustomer = SqlProvider.Db.SelectCustomer();
            return View("_ModifyOldDebt", data);
        }

        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyOldDebtInfo(OldDebt model)
        {

            var ControllerId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyOldDebt(model, ControllerId);

            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteOldDebtById(int id)
        {

            var code = SqlProvider.Db.DeleteOldDebtById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        public ActionResult GetListOldDebt(SearchOldDebt model)
        {
            if (model.CurrentPage == 0)
            {
                model.CurrentPage = 1;
            }
            model.PageSize = 10;
            var data = SqlProvider.Db.GetListOldDebt(model);
            return View("_GetListOldDebt", data);
        }
        #endregion
        #region RevenueType
        [FilterPermission()]
        public ActionResult ModifyListRevenueType(int id)
        {
            RevenueType data = new RevenueType();
            if (id != 0)
            {
                data = SqlProvider.Db.GetRevenueTypeById(id);


            }
            return View("_ModifyListRevenueType", data);
        }

        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyRevenueTypeInfo(RevenueType model)
        {

            var UserId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyRevenueType(model, UserId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteRevenueTypeById(int id)
        {

            var code = SqlProvider.Db.DeleteRevenueTypeById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        public ActionResult GetListRevenueType()
        {
            var data = SqlProvider.Db.SelectTypeRevenue();
            return View("_GetListRevenueType", data);
        }

        [FilterPermission()]
        public ActionResult GetRevenueType(int id)
        {
            var data = SqlProvider.Db.GetRevenueTypeById(id);
            return View("_GetRevenueType", data);
        }
        #endregion
        #region ServiceType
        [FilterPermission()]
        public ActionResult ModifyListServiceType(int id)
        {
            SubscriptionService data = new SubscriptionService();
            if (id != 0)
            {
                data = SqlProvider.Db.GetServiceTypeById(id);


            }
            return View("_ModifyListServiceType", data);
        }

        [FilterPermission()]
        [HttpPost]
        public ActionResult ModifyServiceTypeInfo(SubscriptionService model)
        {

            var UserId = AccountUtil.Id;
            var code = SqlProvider.Db.ModifyServiceType(model, UserId);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        [HttpPost]
        public ActionResult DeleteServiceTypeById(int id)
        {

            var code = SqlProvider.Db.DeleteServiceTypeById(id);
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);

        }
        [FilterPermission()]
        public ActionResult GetListServiceType()
        {
            var data = SqlProvider.Db.SelectTypeService();
            return View("_GetListServiceType", data);
        }

        [FilterPermission()]
        public ActionResult GetServiceType(int id)
        {
            var data = SqlProvider.Db.GetServiceTypeById(id);
            return View("_GetServiceType", data);
        }
        #endregion

    }
}