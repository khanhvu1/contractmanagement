﻿using System.Web;
using System.Web.Optimization;

namespace ContractManagement
{
	public class BundleConfig
	{
		// For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			#region Common
			//style
			bundles.Add(new StyleBundle("~/theme/css").Include(
				   "~/Content/theme/css/bootstrap.min.css",
				   "~/Content/theme/css/animate.css",
				   "~/Content/theme/css/style.css",
				   "~/Content/theme/css/plugins/toastr/toastr.min.css",
				   "~/Content/theme/css/plugins/chosen/bootstrap-chosen.css",
				   "~/Content/theme/css/plugins/switchery/switchery.css",
				   "~/Content/theme/css/plugins/select2/select2.min.css",
				   "~/Content/theme/css/plugins/datapicker/datepicker3.css",
				   "~/Content/theme/css/plugins/daterangepicker/daterangepicker-bs3.css",
				   "~/Content/css/common.css"
			));
			//js
			bundles.Add(new ScriptBundle("~/bundles/script/mainly").Include(
					  "~/Scripts/jquery-3.4.1.min.js",
					  "~/Scripts/jquery-ui.min.js",
					  "~/Scripts/jquery.form.min.js",
					  "~/Content/theme/js/popper.min.js",
					  "~/Content/theme/js/plugins/toastr/toastr.min.js",
					  "~/Content/theme/js/bootstrap.js",
					  "~/Content/theme/js/plugins/chosen/chosen.jquery.js",
					  "~/Content/theme/js/plugins/metisMenu/jquery.metisMenu.js",
					  "~/Content/theme/js/plugins/slimscroll/jquery.slimscroll.min.js",
					  "~/Scripts/jquery.validate.min.js",
					  "~/Content/theme/js/plugins/switchery/switchery.js",
					  "~/Content/theme/js/plugins/select2/select2.full.min.js",
					  "~/Content/theme/js/plugins/datapicker/bootstrap-datepicker.js",
					  "~/Content/theme/js/plugins/daterangepicker/daterangepicker.js",
					  "~/Content/js/common/common.js",
					  "~/Content/theme/js/plugins/flot/jquery.flot.js",
					  "~/Content/theme/js/plugins/flot/jquery.flot.tooltip.min.js",
					  "~/Content/theme/js/plugins/flot/jquery.flot.resize.js",
					  "~/Content/theme/js/plugins/flot/ jquery.flot.pie.js",
					  "~/Content/theme/js/plugins/flot/jquery.flot.time.js"

					  ));
			#endregion

			#region load-index
			bundles.Add(new ScriptBundle("~/js/load-index").Include(
					  "~/Content/js/loadpage/load-index.js"
					  ));
			#endregion
			#region load-listCustomer
			bundles.Add(new ScriptBundle("~/js/load-listCustomer").Include(
					  "~/Content/js/loadpage/load-listCustomer.js"
					  ));
            #endregion
            #region load-customer
            bundles.Add(new ScriptBundle("~/js/load-customer").Include(
                      "~/Content/js/loadpage/load-customer.js"
                      ));
			#endregion
			#region load-modifyCustomer
			bundles.Add(new ScriptBundle("~/js/load-modifyCustomer").Include(
					  "~/Content/js/loadpage/load-modifyCustomer.js"
					  ));
			#endregion
			#region load-login
			bundles.Add(new ScriptBundle("~/js/load-login").Include(
					  "~/Content/js/loadpage/load-login.js"
					  ));
			#endregion
			#region load-revenueDebt
			bundles.Add(new ScriptBundle("~/js/load-revenueDebt").Include(
					  "~/Content/js/loadpage/load-revenueDebt.js"
					  ));
			#endregion
			#region load-revenueType
			bundles.Add(new ScriptBundle("~/js/load-revenueType").Include(
					  "~/Content/js/loadpage/load-revenueType.js"
					  ));
			#endregion
			#region load-serviceType
			bundles.Add(new ScriptBundle("~/js/load-serviceType").Include(
					  "~/Content/js/loadpage/load-serviceType.js"
					  ));
			#endregion
			#region load-contract
			bundles.Add(new ScriptBundle("~/js/load-contract").Include(
					  "~/Content/js/loadpage/load-contract.js"
					  ));
			#endregion
			#region load-modifyContract
			bundles.Add(new ScriptBundle("~/js/load-modifyContract").Include(
					  "~/Content/js/loadpage/load-modifyContract.js"
					  ));
			#endregion
			#region load-listStaff
			bundles.Add(new ScriptBundle("~/js/load-listStaff").Include(
					  "~/Content/js/loadpage/load-listStaff.js"
					  ));
			#endregion
			#region load-staff
			bundles.Add(new ScriptBundle("~/js/load-staff").Include(
					  "~/Content/js/loadpage/load-staff.js"
					  ));
			#endregion
			#region load-modifyStaff
			bundles.Add(new ScriptBundle("~/js/load-modifyStaff").Include(
					  "~/Content/js/loadpage/load-modifyStaff.js"
					  ));
			#endregion
			#region login
			bundles.Add(new ScriptBundle("~/js/login").Include(
					  "~/Content/js/common/login.js"
					  ));
			#endregion
			#region load-contractTracking
			bundles.Add(new ScriptBundle("~/js/load-contractTracking").Include(
					  "~/Content/js/loadpage/load-contractTracking.js"
					  ));
			#endregion
			#region load-contractTrackingProfile
			bundles.Add(new ScriptBundle("~/js/load-contractTrackingProfile").Include(
					  "~/Content/js/loadpage/load-contractTrackingProfile.js"
					  ));
			#endregion
			#region load-modifyContractTracking
			bundles.Add(new ScriptBundle("~/js/load-modifyContractTracking").Include(
					  "~/Content/js/loadpage/load-modifyContractTracking.js"
					  ));
			#endregion
			#region load-listOldDebt
			bundles.Add(new ScriptBundle("~/js/load-listOldDebt").Include(
					  "~/Content/js/loadpage/load-listOldDebt.js"
					  ));
			#endregion
		}
	}
}
