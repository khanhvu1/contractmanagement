﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Models
{
    public class SearchOldDebt:Search
    {
        public string Key { get; set; }
        public List<OldDebtModel> Result { get; set; }
    }
}