﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Models
{
    public class Search
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalRecord { get; set; }
        public int PageNumber { get; set; }
        public int TotalPage
        {
            get
            {
                if (TotalRecord <= 0)
                    return 0;
                var _totalPage = TotalRecord / PageSize;
                if (TotalRecord % PageSize != 0)
                    _totalPage++;
                return _totalPage;
            }
        }
    }
}