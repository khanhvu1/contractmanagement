﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Models
{
    public class SearchRevenueDebt:Search
    {
        public string Key { get; set; }
        public int? Year { get; set; }
        public List<RevenueDebtModel> Result { get; set; }
    }
}