﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Models
{
    public class ListStaffModel:ListStaff
    {
        public decimal Kpi1 { get; set; }
        public decimal Kpi2 { get; set; }
        public decimal Kpi3 { get; set; }
        public decimal Kpi4 { get; set; }
        public decimal Kpi5 { get; set; }
        public decimal Kpi6 { get; set; }
        public decimal Kpi7 { get; set; }
        public decimal Kpi8 { get; set; }
        public decimal Kpi9 { get; set; }
        public decimal Kpi10 { get; set; }
        public decimal Kpi11 { get; set; }
        public decimal Kpi12 { get; set; }
        public int Year { get; set; }
        public int KPIId { get; set; }
        public decimal TotalRevenue1 { get; set; }
        public decimal TotalRevenue2 { get; set; }
        public decimal TotalRevenue3 { get; set; }
        public decimal TotalRevenue4 { get; set; }
        public decimal TotalRevenue5 { get; set; }
        public decimal TotalRevenue6 { get; set; }
        public decimal TotalRevenue7 { get; set; }
        public decimal TotalRevenue8 { get; set; }
        public decimal TotalRevenue9 { get; set; }
        public decimal TotalRevenue10 { get; set; }
        public decimal TotalRevenue11 { get; set; }
        public decimal TotalRevenue12 { get; set; }
        public decimal TotalRevenueSum { get; set; }

    }
}