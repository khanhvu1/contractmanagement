﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Models
{
    public class RevenueDebtModel:RevenueDebt
    {
        public string CustomerName { get; set; }
        public string UserName { get; set; }
        public decimal DebtValue { get; set; }
        public decimal Receivables { get; set; }
        public decimal TotalDebtCollected { get; set; }

    }
}