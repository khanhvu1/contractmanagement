﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Models
{
	public class Customers:Customer
	{
		public string IndustryName { get; set; }
		public string ContactPersons { get; set; }
		public decimal RevenueYear { get; set; }
		public decimal RevenueLastYear { get; set; }
		public decimal RevenueTwoYearsAgo { get; set; }

	}
}