﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Models
{
    public class SearchStaff:Search
    {

        public string Key { get; set; }
        public int? Year { get; set; }
        public List<ListStaffModel> Result { get; set; }


    }
}