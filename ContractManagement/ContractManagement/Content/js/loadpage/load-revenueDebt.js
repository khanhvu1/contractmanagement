﻿revenueDebt = {
    init: function () {
        revenueDebt.bindEvents();
        revenueDebt.loadRevenueDebt();
    },
    loadRevenueDebt: function () {
        $.get("/Home/RevenueDebt", {}, function (response) {
            common.hideLoading();
            $('.add-contract').click(function () {
                var year = $(this).attr('data-year');
                console.log(year);
                $.post("/Home/ModifyContractInfo", { RevenueDebtId: 0,year:year }, function (data) {

                    location.href = "/Home/ModifyContract?id=" + data.Code + "&year=" + year;
                });
            });
            $('.view-contract').click(function () {
                let id = $(this).attr('data-id');
                let year = $(this).attr('data-year');
                location.href = "/Home/GetContract?id=" + id + "&year=" + year;
            });
            $('a.btn.btn-primary').click(function () {
                var year = $(this).data('year');
                var url = $(this).attr('href') + '?year=' + year;
                window.location.href = url;
            });
            $(".delete-contract").click(function () {
                let id = $(this).attr('data-id');
                let this_ = $(this);

                if (confirm("Bạn chắc chắn muốn xóa khách hàng này?") == true) {
                    $.post("/Home/DeleteContractById", { id: id }, function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Xóa thành công !");
                            this_.closest("tr").remove(); // Xóa dòng dữ liệu khỏi bảng HTML
                        } else {
                            common.openToastFail("Xóa thất bại");
                        }
                    }).done(function () {
                        // Thực hiện các hành động cần thiết sau khi xóa, nếu có
                    });
                }
            });
        });
    },
    bindEvents: function () {
        // Lưu giá trị được chọn từ dropdown list "Năm" vào localStorage khi thực hiện tìm kiếm
        $('#form_search').submit(function (event) {
            var selectedYear = $('#selectedYear').val();
            localStorage.setItem('selectedYear', selectedYear);
        });

        // Đặt lại giá trị đã chọn trong dropdown list "Năm" khi trang được tải lại sau khi tìm kiếm
        var storedYear = localStorage.getItem('selectedYear');
        if (storedYear) {
            $('#selectedYear').val(storedYear);
        }
    }
};

$(function () {
    revenueDebt.init();
});
