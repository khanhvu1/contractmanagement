﻿modifyCustomer = {
    init: function () {
        var customerID = $("#CustomerID").val();
        common.showLoading();

        // Gửi AJAX request để lấy dữ liệu khách hàng
        $.get("/Home/ModifyCustomer", { id: customerID }, function (response) {
            common.hideLoading();

            // Khởi tạo các biến để lưu giữ các giá trị nhập trước đó
            var lastCustomerName = sessionStorage.getItem('lastCustomerName') || "";
            var lastIndustryID = sessionStorage.getItem('lastIndustryID') || "";
            var lastAddress = sessionStorage.getItem('lastAddress') || "";
            var lastTaxCode = sessionStorage.getItem('lastTaxCode') || "";
            var lastPhone = sessionStorage.getItem('lastPhone') || "";
            var lastEmail = sessionStorage.getItem('lastEmail') || "";
            var lastFoundationDate = sessionStorage.getItem('lastFoundationDate') || "";


            // Xử lý khi form edit được load thành công
            $("#editForm").validate({
                rules: {
                    CustomerName: { required: true },
                    IndustryID: { required: true },
                    Address: { required: true },
                    TaxCode: { required: true },
                    Phone: { required: true },
                    Email: { required: true },
                    FoundationDate: { required: true },
                },
                messages: {
                    // Định nghĩa các thông báo lỗi cho từng trường
                    CustomerName: { required: "Yêu cầu đủ thông tin!" },
                    IndustryID: { required: "Yêu cầu đủ thông tin!" },
                    Address: { required: "Yêu cầu đủ thông tin!" },
                    TaxCode: { required: "Yêu cầu đủ thông tin!" },
                    Phone: { required: "Yêu cầu đủ thông tin!" },
                    Email: { required: "Yêu cầu đủ thông tin!" },
                    FoundationDate: { required: "Yêu cầu đủ thông tin!" },
                }
            });

            // Xử lý khi submit form edit
            $("#editForm").ajaxForm({
                beforeSubmit: function () { },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", "");
                        common.closeModal();
                        location.reload(true);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });

            $('.modify-contactPerson').click(function () {
                let dataid = $(this).attr("data-id");
                modifyCustomer.ModifyContactPerson(dataid, customerID);
            });
            // Xử lý khi click vào nút "Thêm người liên hệ"
            $('.add-contactPerson').click(function () {
                // Lưu lại các giá trị nhập trước đó
                sessionStorage.setItem('lastCustomerName', $('#customerName').val());
                sessionStorage.setItem('lastIndustryID', $('#IndustryID').val());
                sessionStorage.setItem('lastAddress', $('#address').val());
                sessionStorage.setItem('lastTaxCode', $('#taxCode').val());
                sessionStorage.setItem('lastPhone', $('#phone').val());
                sessionStorage.setItem('lastEmail', $('#email').val());
                sessionStorage.setItem('lastFoundationDate', $('#foundationDate').val());


                // Gọi hàm ModifyContactPerson với tham số id = 0 và customerID
                modifyCustomer.ModifyContactPerson(0, customerID);
            });

            // Gán lại các giá trị nhập trước đó vào các trường input
            if (lastCustomerName != "") {
                $('#customerName').val(lastCustomerName);
            }
            if (lastIndustryID != "") {
                $('#IndustryID').val(lastIndustryID);
            }
            if (lastAddress != "") {
                $('#address').val(lastAddress);
            }
            if (lastTaxCode != "") {

                $('#taxCode').val(lastTaxCode);
            }
            if (lastPhone != "") {

                $('#phone').val(lastPhone);
            }
            if (lastEmail != "") {

                $('#email').val(lastEmail);
            }
            if (lastFoundationDate != "") {
                $('#foundationDate').val(lastFoundationDate);
            }
            sessionStorage.removeItem('lastCustomerName');
            sessionStorage.removeItem('lastIndustryID');
            sessionStorage.removeItem('lastAddress');
            sessionStorage.removeItem('lastTaxCode');
            sessionStorage.removeItem('lastPhone');
            sessionStorage.removeItem('lastEmail');
            sessionStorage.removeItem('lastFoundationDate');
            // Xử lý khi click vào nút "Xóa người liên hệ"
            $(".delete-contactPerson").click(function () {
                let id = $(this).attr('data-id');
                let this_ = $(this);

                if (confirm("Bạn chắc chắn muốn xóa người liên hệ này?") == true) {
                    $.post("/Home/DeletecontactPersonInfoById", { id: id }, function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !");
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    }).done(function () {
                        location.reload(true);
                    });
                }
            });

            // Xử lý khi click vào nút "Quay lại"
            $('.btn.btn-white[data-dismiss="modal"]').click(function () {
                let customerID = $("#CustomerID").val();
                let newUrl = "";
                if (customerID == 0) {
                    newUrl = "/Home/GetListCustomer";
                } else {
                    newUrl = "/Home/GetCustomer?id=" + customerID;
                }
                window.location.href = newUrl;
            });
        });
    },

    ModifyContactPerson: function (id, customerID) {
        common.showLoading();
        $.get("/Home/ModifyContactPerson", { id: id }, function (response) {
            common.hideLoading();
            common.appendModal(response);
            if (id == 0) {
                $("#contactPersonCustomerID").val(customerID);
            }
            $("#editFormContactPerson").ajaxForm({
                beforeSubmit: function () { },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", "");
                        common.closeModal();
                        location.reload(true);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });
        });
    },
};

$(function () {
    modifyCustomer.init();
});
