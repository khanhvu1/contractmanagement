﻿modifyContract = {
    init: function () {
        var revenueDebtId = $("#revenueDebtId").val();
        var year = $("#year").val();
        common.showLoading();

        $.get("/Home/ModifyContract", { id: revenueDebtId,year:year }, function (response) {
            common.hideLoading();
            // Khởi tạo các biến để lưu giữ các giá trị nhập trước đó
            var lastCustomerID = sessionStorage.getItem('lastCustomerID') || "";
            var lastContractNumber = sessionStorage.getItem('lastContractNumber') || "";
            var lastSigningDate = sessionStorage.getItem('lastSigningDate') || "";
            var lastExpiryDate = sessionStorage.getItem('lastExpiryDate') || "";
            var lastContractValue = sessionStorage.getItem('lastContractValue') || "";
            var lastUserId = sessionStorage.getItem('lastUserId') || "";
            var lastYear = sessionStorage.getItem('lastYear') || "";
            var lastRevenue1 = sessionStorage.getItem('lastRevenue1') || "";
            var lastRevenue2 = sessionStorage.getItem('lastRevenue2') || "";
            var lastRevenue3 = sessionStorage.getItem('lastRevenue3') || "";
            var lastRevenue4 = sessionStorage.getItem('lastRevenue4') || "";
            var lastRevenue5 = sessionStorage.getItem('lastRevenue5') || "";
            var lastRevenue6 = sessionStorage.getItem('lastRevenue6') || "";
            var lastRevenue7 = sessionStorage.getItem('lastRevenue7') || "";
            var lastRevenue8 = sessionStorage.getItem('lastRevenue8') || "";
            var lastRevenue9 = sessionStorage.getItem('lastRevenue9') || "";
            var lastRevenue10 = sessionStorage.getItem('lastRevenue10') || "";
            var lastRevenue11 = sessionStorage.getItem('lastRevenue11') || "";
            var lastRevenue12 = sessionStorage.getItem('lastRevenue12') || "";

            // Xử lý khi form edit được load thành công
            $("#editForm").validate({
                rules: {
                    CustomerID: { required: true },
                    ContractNumber: { required: true },
                    SigningDate: { required: true },
                    ExpiryDate: { required: true },
                    ContractValue: { required: true },
                    UserId: { required: true },
                    Year: { required: true },
                    //Revenue1: { required: true },
                    //Revenue2: { required: true },
                    //Revenue3: { required: true },
                    //Revenue4: { required: true },
                    //Revenue5: { required: true },
                    //Revenue6: { required: true },
                    //Revenue7: { required: true },
                    //Revenue8: { required: true },
                    //Revenue9: { required: true },
                    //Revenue10: { required: true },
                    //Revenue11: { required: true },
                    //Revenue12: { required: true },
                },
                messages: {
                    // Định nghĩa các thông báo lỗi cho từng trường
                    CustomerID: { required: "Yêu cầu đủ thông tin!" },
                    ContractNumber: { required: "Yêu cầu đủ thông tin!" },
                    SigningDate: { required: "Yêu cầu đủ thông tin!" },
                    ExpiryDate: { required: "Yêu cầu đủ thông tin!" },
                    ContractValue: { required: "Yêu cầu đủ thông tin!" },
                    UserId: { required: "Yêu cầu đủ thông tin!" },
                    Year: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue1: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue2: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue3: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue4: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue5: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue6: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue7: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue8: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue9: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue10: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue11: { required: "Yêu cầu đủ thông tin!" },
                    //Revenue12: { required: "Yêu cầu đủ thông tin!" },

                }
            });

            $("#editForm").ajaxForm({
                beforeSubmit: function () { },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", "");
                        common.closeModal();
                        location.reload(true);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });

            $('.modify-revenue').click(function () {
                let dataid = $(this).attr("data-id");
                modifyContract.ModifyRevenue(dataid, revenueDebtId);
            });
            $('.modify-debt').click(function () {
                let dataid = $(this).attr("data-id");
                modifyContract.ModifyDebt(dataid, revenueDebtId);
            });

            $('.add-revenue').click(function () {
                // Lưu lại các giá trị nhập trước đó
                sessionStorage.setItem('lastCustomerID', $('#customerID').val());
                sessionStorage.setItem('lastContractNumber', $('#contractNumber').val());
                sessionStorage.setItem('lastSigningDate', $('#signingDate').val());
                sessionStorage.setItem('lastExpiryDate', $('#expiryDate').val());
                sessionStorage.setItem('lastContractValue', $('#contractValue').val());
                sessionStorage.setItem('lastUserId', $('#userId').val());
                sessionStorage.setItem('lastYear', $('#year').val());
                sessionStorage.setItem('lastRevenue1', $('#revenue1').val());
                sessionStorage.setItem('lastRevenue2', $('#revenue2').val());
                sessionStorage.setItem('lastRevenue3', $('#revenue3').val());
                sessionStorage.setItem('lastRevenue4', $('#revenue4').val());
                sessionStorage.setItem('lastRevenue5', $('#revenue5').val());
                sessionStorage.setItem('lastRevenue6', $('#revenue6').val());
                sessionStorage.setItem('lastRevenue7', $('#revenue7').val());
                sessionStorage.setItem('lastRevenue8', $('#revenue8').val());
                sessionStorage.setItem('lastRevenue9', $('#revenue9').val());
                sessionStorage.setItem('lastRevenue10', $('#revenue10').val());
                sessionStorage.setItem('lastRevenue11', $('#revenue11').val());
                sessionStorage.setItem('lastRevenue12', $('#revenue12').val());
                
                modifyContract.ModifyRevenue(0, revenueDebtId);
            });
            $('.add-debt').click(function () {
                // Lưu lại các giá trị nhập trước đó
                sessionStorage.setItem('lastCustomerID', $('#customerID').val());
                sessionStorage.setItem('lastContractNumber', $('#contractNumber').val());
                sessionStorage.setItem('lastSigningDate', $('#signingDate').val());
                sessionStorage.setItem('lastExpiryDate', $('#expiryDate').val());
                sessionStorage.setItem('lastContractValue', $('#contractValue').val());
                sessionStorage.setItem('lastUserId', $('#userId').val());
                sessionStorage.setItem('lastYear', $('#year').val());
                sessionStorage.setItem('lastRevenue1', $('#revenue1').val());
                sessionStorage.setItem('lastRevenue2', $('#revenue2').val());
                sessionStorage.setItem('lastRevenue3', $('#revenue3').val());
                sessionStorage.setItem('lastRevenue4', $('#revenue4').val());
                sessionStorage.setItem('lastRevenue5', $('#revenue5').val());
                sessionStorage.setItem('lastRevenue6', $('#revenue6').val());
                sessionStorage.setItem('lastRevenue7', $('#revenue7').val());
                sessionStorage.setItem('lastRevenue8', $('#revenue8').val());
                sessionStorage.setItem('lastRevenue9', $('#revenue9').val());
                sessionStorage.setItem('lastRevenue10', $('#revenue10').val());
                sessionStorage.setItem('lastRevenue11', $('#revenue11').val());
                sessionStorage.setItem('lastRevenue12', $('#revenue12').val());

                modifyContract.ModifyDebt(0, revenueDebtId);
            });
            // Gán lại các giá trị nhập trước đó vào các trường input
            if (lastCustomerID != "") {
                $('#customerID').val(lastCustomerID);
            }
            if (lastContractNumber != "") {
                $('#contractNumber').val(lastContractNumber);
            }
            if (lastSigningDate != "") {
                $('#signingDate').val(lastSigningDate);
            }
            if (lastExpiryDate != "") {

                $('#expiryDate').val(lastExpiryDate);
            }
            if (lastContractValue != "") {

                $('#contractValue').val(lastContractValue);
            }
            if (lastUserId != "") {

                $('#userId').val(lastUserId);
            }
            if (lastContractValue != "") {

                $('#contractValue').val(lastContractValue);
            }
            if (lastYear != "") {

                $('#year').val(lastYear);
            }
            if (lastRevenue1 != "") {
                $('#revenue1').val(lastRevenue1);
            }
            if (lastRevenue2 != "") {
                $('#revenue2').val(lastRevenue2);
            }
            if (lastRevenue3 != "") {
                $('#revenue3').val(lastRevenue3);
            }
            if (lastRevenue4 != "") {
                $('#revenue4').val(lastRevenue4);
            }
            if (lastRevenue5 != "") {
                $('#revenue5').val(lastRevenue5);
            }
            if (lastRevenue6 != "") {
                $('#revenue6').val(lastRevenue6);
            }
            if (lastRevenue7 != "") {
                $('#revenue7').val(lastRevenue7);
            }
            if (lastRevenue8 != "") {
                $('#revenue8').val(lastRevenue8);
            }
            if (lastRevenue9 != "") {
                $('#revenue9').val(lastRevenue9);
            }
            if (lastRevenue10 != "") {
                $('#revenue10').val(lastRevenue10);
            }
            if (lastRevenue11 != "") {
                $('#revenue11').val(lastRevenue11);
            }
            if (lastRevenue12 != "") {
                $('#revenue12').val(lastRevenue12);
            }
            // Xóa các giá trị đã lưu trong sessionStorage
            sessionStorage.removeItem('lastCustomerID');
            sessionStorage.removeItem('lastContractNumber');
            sessionStorage.removeItem('lastSigningDate');
            sessionStorage.removeItem('lastExpiryDate');
            sessionStorage.removeItem('lastContractValue');
            sessionStorage.removeItem('lastUserId');
            sessionStorage.removeItem('lastYear');
            sessionStorage.removeItem('lastRevenue1');
            sessionStorage.removeItem('lastRevenue2');
            sessionStorage.removeItem('lastRevenue3');
            sessionStorage.removeItem('lastRevenue4');
            sessionStorage.removeItem('lastRevenue5');
            sessionStorage.removeItem('lastRevenue6');
            sessionStorage.removeItem('lastRevenue7');
            sessionStorage.removeItem('lastRevenue8');
            sessionStorage.removeItem('lastRevenue9');
            sessionStorage.removeItem('lastRevenue10');
            sessionStorage.removeItem('lastRevenue11');
            sessionStorage.removeItem('lastRevenue12');
            $(".delete-revenue").click(function () {
                let id = $(this).attr('data-id');
                let this_ = $(this);

                if (confirm("Bạn chắc chắn muốn xóa doanh thu này ?") == true) {
                    $.post("/Home/DeleteRevenueById", { id: id }, function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !");
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    }).done(function () {
                        location.reload(true);
                    });
                }
            });
            $(".delete-debt").click(function () {
                let id = $(this).attr('data-id');
                let this_ = $(this);

                if (confirm("Bạn chắc chắn muốn xóa công nợ này ?") == true) {
                    $.post("/Home/DeleteDebtById", { id: id }, function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !");
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    }).done(function () {
                        location.reload(true);
                    });
                }
            });
            $('.btn.btn-white[data-dismiss="modal"]').click(function () {
                var revenueDebtId = $("#revenueDebtId").val();
                var year = $("#year").val();
                let newUrl = "";
                if (revenueDebtId == 0) {
                    newUrl = "/Home/RevenueDebt";
                }
                else {
                    newUrl = "/Home/GetContract?id=" + revenueDebtId + "&year=" + year;
                }
                window.location.href = newUrl;
            });

        });
        var selectElement = document.querySelector('select[name="Year"]');
        var currentYear = new Date().getFullYear();
        for (var i = currentYear - 5; i <= currentYear + 5; i++) {
            var option = document.createElement('option');
            option.value = i;
            option.textContent = i;
            selectElement.appendChild(option);
        }
    },

    ModifyRevenue: function (id, revenueDebtId) {
        common.showLoading();
        $.get("/Home/ModifyRevenue", { id: id }, function (response) {
            common.hideLoading();
            common.appendModal(response);
            if (id == 0) {
                $("#revenueRevenueDebtId").val(revenueDebtId);
            }
            $("#editFormRevenue").ajaxForm(
                {
                    beforeSubmit: function () {
                    },
                    success: function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !", "");
                            common.closeModal();
                            location.reload(true);
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    },
                    error: function () {
                        Loading.close();
                    }
                });

        });
    },
    ModifyDebt: function (id, revenueDebtId) {
        common.showLoading();
        $.get("/Home/ModifyDebt", { id: id }, function (response) {
            common.hideLoading();
            common.appendModal(response);
            if (id == 0) {
                $("#debtRevenueDebtId").val(revenueDebtId);
            }
            $("#editFormDebt").ajaxForm(
                {
                    beforeSubmit: function () {
                    },
                    success: function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !", "");
                            common.closeModal();
                            location.reload(true);
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    },
                    error: function () {
                        Loading.close();
                    }
                });

        });
    },
},


    $(function () {
        modifyContract.init();
    });


