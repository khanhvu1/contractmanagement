﻿listCustomer = {
    init: function () {
        $.get("/Home/GetListCustomer", {}, function (response) {
            common.hideLoading();
            $('.add-customer').click(function () {
                $.post("/Home/ModifyCustomerInfo", { CustomerID: 0 }, function (data) {

                    location.href = "/Home/ModifyCustomer?id=" + data.Code;
                });
            });
            $('.view-customer').click(function (event) {
                let id = $(this).attr('data-id');
                location.href = "/Home/GetCustomer?id=" + id;
            });
            $(".delete-customer").click(function () {
                let id = $(this).attr('data-id');
                let this_ = $(this);

                if (confirm("Bạn chắc chắn muốn xóa khách hàng này?") == true) {
                    $.post("/Home/DeleteCustomerById", { id: id }, function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !");
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    }).done(function () {
                        location.reload(true);
                    });
                }
            });
        },
        )
    },


};
$(function () {
    listCustomer.init();
});
