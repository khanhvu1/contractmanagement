﻿staff = {
    init: function () {
        let id = $("#id").val();
        common.showLoading();
        $.get("/Home/GetStaff", { id: id }, function (response) {
            common.hideLoading();
            $('.modify-kpi').click(function () {
                let id = $(this).attr("data-id");

                staff.ModifyKpi(id);
            });

        });

        // Đoạn mã thêm vào
        $(document).ready(function () {
            var storedYear = localStorage.getItem("selectedYear");
            var currentYear = (new Date()).getFullYear(); // Lấy năm hiện tại
            var displayYear = storedYear ? storedYear : currentYear; // Sử dụng năm lưu trữ hoặc năm hiện tại nếu không có năm lưu trữ
            $('#yearSpan').text(displayYear);
        });
        document.getElementById('goBackButton').addEventListener('click', function () {
            history.back();
        });
    },
    ModifyKpi: function (id, ) {
        common.showLoading();
        $.get("/Home/ModifyKpi", { id: id }, function (response) {
            common.hideLoading();
            common.appendModal(response);
            $("#editForm").ajaxForm({
                beforeSubmit: function () { },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", "");
                        common.closeModal();
                        location.reload(true);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });
            $('.btn.btn-white[data-dismiss="modal"]').click(function () {
                let id = $(this).attr("data-id");
                location.href = "/Home/GetStaff?id=" + id;
            });
        });
    },
};

$(function () {
    staff.init();
});
