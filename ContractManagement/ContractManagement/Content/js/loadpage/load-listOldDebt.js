﻿listOldDebt = {
    init: function () {
        $.get("/Home/GetListOldDebt", {}, function (response) {
            common.hideLoading();
            $('.add-oldDebt').click(function () {
                listOldDebt.ModifyOldDebt(0);
                });
            });
            $('.modify-oldDebt').click(function (event) {
                let id = $(this).attr('data-id');
                listOldDebt.ModifyOldDebt(id);
            });
            $(".delete-oldDebt").click(function () {
                let id = $(this).attr('data-id');
                let this_ = $(this);

                if (confirm("Bạn chắc chắn muốn xóa nợ cũ này?") == true) {
                    $.post("/Home/DeleteOldDebtById", { id: id }, function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !");
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    }).done(function () {
                        location.reload(true);
                    });
                }
            });
    },
    ModifyOldDebt: function (id) {
        common.showLoading();
        $.get("/Home/ModifyOldDebt", { id: id }, function (response) {
            common.hideLoading();
            common.appendModal(response);
            $("#editForm").ajaxForm({
                beforeSubmit: function () { },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", "");
                        common.closeModal();
                        location.reload(true);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });
            $('.btn.btn-white[data-dismiss="modal"]').click(function () {
                window.location.href = "/Home/GetListOldDebt";
            });
        });
    },
};
$(function () {
    listOldDebt.init();
});
