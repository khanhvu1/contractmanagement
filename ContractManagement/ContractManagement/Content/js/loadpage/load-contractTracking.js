﻿contractTracking = {
    init: function () {
        contractTracking.bindEvents();
        contractTracking.GetContractTracking();
    },
    GetContractTracking: function () {
        $.get("/Home/ContractTracking", {}, function (response) {
            common.hideLoading();
            $('.view-ContractTracking').click(function () {
                let id = $(this).attr("data-id");
                location.href = "/Home/GetContractTracking?id=" + id;
            });
        },
        )  
    },

    bindEvents: function () {
        // Lưu giá trị được chọn từ dropdown list "Năm" vào localStorage khi thực hiện tìm kiếm
        $('#form_search').submit(function (event) {
            var selectedYear = $('#selectedYear').val();
            localStorage.setItem('selectedYear', selectedYear);
        });
        $('a.btn.btn-primary').click(function () {
            var year = $(this).data('year');
            var url = $(this).attr('href') + '?year=' + year;
            window.location.href = url;
        });
        // Đặt lại giá trị đã chọn trong dropdown list "Năm" khi trang được tải lại sau khi tìm kiếm
        var storedYear = localStorage.getItem('selectedYear');
        if (storedYear) {
            $('#selectedYear').val(storedYear);
        }
    }
};

$(function () {
    contractTracking.init();
});



