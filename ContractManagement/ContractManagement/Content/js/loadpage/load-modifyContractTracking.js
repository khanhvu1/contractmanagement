﻿modifyContractTracking = {
    init: function () {
        var contractTrackingId = $("#contractTrackingId").val();
        common.showLoading();

        $.get("/Home/ModifyContractTracking", { id: contractTrackingId }, function (response) {
            common.hideLoading();

            $("#editForm").ajaxForm({
                beforeSubmit: function () { },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", "");
                        common.closeModal();
                        location.reload(true);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });

            $('.modify-service').click(function () {
                let dataid = $(this).attr("data-id");
                modifyContractTracking.ModifyService(dataid, contractTrackingId);
            });

            $('.add-service').click(function () {
                  modifyContractTracking.ModifyService(0, contractTrackingId);
            });
            $(".delete-service").click(function () {
                let id = $(this).attr('data-id');
                let this_ = $(this);

                if (confirm("Bạn chắc chắn muốn xóa dịch vụ này ?") == true) {
                    $.post("/Home/DeleteServiceById", { id: id }, function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !");
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    }).done(function () {
                        location.reload(true);
                    });
                }
            });
            $('.btn.btn-white[data-dismiss="modal"]').click(function () {
                var contractTrackingId = $("#contractTrackingId").val();
                let newUrl = "";
                if (contractTrackingId == 0) {
                    newUrl = "/Home/ContractTracking";
                }
                else {
                    newUrl = "/Home/GetContractTracking?id=" + contractTrackingId;
                }
                window.location.href = newUrl;
            });

        });
        //var selectElement = document.querySelector('select[name="Year"]');
        //var currentYear = new Date().getFullYear();
        //for (var i = currentYear - 5; i <= currentYear + 5; i++) {
        //    var option = document.createElement('option');
        //    option.value = i;
        //    option.textContent = i;
        //    selectElement.appendChild(option);
        //}
    },

    ModifyService: function (id, contractTrackingId) {
        common.showLoading();
        $.get("/Home/ModifyService", { id: id }, function (response) {
            common.hideLoading();
            common.appendModal(response);
            if (id == 0) {
                $("#serviceContractTrackingId").val(contractTrackingId);
            }
            $("#editFormService").ajaxForm(
                {
                    beforeSubmit: function () {
                    },
                    success: function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !", "");
                            common.closeModal();
                            location.reload(true);
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    },
                    error: function () {
                        Loading.close();
                    }
                });

        });
    },
},


    $(function () {
        modifyContractTracking.init();
    });


