﻿revenueType = {
    init: function () {
        revenueType.GetListRevenueType();
    },
    GetListRevenueType: function () {
        $.get("/Home/GetListRevenueType", function (response) {
            $("#content").html(response);//Chèn file html vào id "#content"

            $('.add-revenueType').click(function () {
                revenueType.ModifyListRevenueType(0);
            });
            $('.modify-revenueType').click(function () {
                let dataid = $(this).attr("data-id");
                revenueType.ModifyListRevenueType(dataid);
            });

            $(".delete-revenueType").click(function () {
                let id = $(this).attr('data-id');

                if (confirm("Bạn chắc chắn muốn xóa loại doanh thu này?") == true) {
                    $.post("/Home/DeleteRevenueTypeById", { id: id }, function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !");
                            /*revenueType.GetListRevenueType();*/
                            location.reload(true);
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    });
                }

            });

        });
    },

    ModifyListRevenueType: function (id) {
        common.showLoading();
        $.get("/Home/ModifyListRevenueType", { id: id }, function (response) {
            common.hideLoading();
            common.appendModal(response);
            $("#editForm").ajaxForm(
                {
                    beforeSubmit: function () {
                    },
                    success: function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !", "");
                            common.closeModal();
                            location.reload(true);
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    },
                    error: function () {
                        Loading.close();
                    }
                });

        });
    },
};
$(function () {
    revenueType.init();
});



