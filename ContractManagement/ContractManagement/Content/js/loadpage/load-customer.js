﻿customer = {
    init: function () {
        let customerId = $("#customerId").val();
        common.showLoading();
        $.get("/Home/GetCustomer", { id: customerId }, function (response) {
            common.hideLoading();
            $('.modify-customer').click(function () {
                let id = $(this).attr('data-id');
                location.href = "/Home/ModifyCustomer?id=" + id;
            });

            // Gán sự kiện quay lại trang trước cho nút Quay lại
            document.getElementById('goBackButton').addEventListener('click', function () {
                window.location.href = "/Home/GetListCustomer";
            });
        });
    },
};

$(function () {
    customer.init();
});
