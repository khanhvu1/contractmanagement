﻿contract = {
    init: function () {
        let revenueDebtId = $("#revenueDebtId").val();
        let year = $("#year").val();

        common.showLoading();
        $.get("/Home/GetContract", { id: revenueDebtId,year:year }, function (response) {
            common.hideLoading();
            $('.modify-contract').click(function () {
                let id = $(this).attr('data-id');
                let year = $(this).attr('data-year');
                console.log(year)
                location.href = "/Home/ModifyContract?id=" + id + "&year=" + year;
            });
            document.getElementById('goBackButton').addEventListener('click', function () {
                window.location.href = "/Home/RevenueDebt";
            });
        });
    },

};
$(function () {
    contract.init();
});



