﻿serviceType = {
    init: function () {
        serviceType.GetListServiceType();
    },
    GetListServiceType: function () {
        $.get("/Home/GetListServiceType", function (response) {
            $("#content").html(response);//Chèn file html vào id "#content"

            $('.add-serviceType').click(function () {
                serviceType.ModifyListServiceType(0);
            });
            $('.modify-serviceType').click(function () {
                let dataid = $(this).attr("data-id");
                serviceType.ModifyListServiceType(dataid);
            });

            $(".delete-serviceType").click(function () {
                let id = $(this).attr('data-id');

                if (confirm("Bạn chắc chắn muốn xóa dịch vụ này?") == true) {
                    $.post("/Home/DeleteServiceTypeById", { id: id }, function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !");
                            /*ServiceType.GetListServiceType();*/
                            location.reload(true);
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    });
                }

            });

        });
    },

    ModifyListServiceType: function (id) {
        common.showLoading();
        $.get("/Home/ModifyListServiceType", { id: id }, function (response) {
            common.hideLoading();
            common.appendModal(response);
            $("#editForm").ajaxForm(
                {
                    beforeSubmit: function () {
                    },
                    success: function (data) {
                        if (data.Code > 0) {
                            common.openToastSuccess("Cập nhật thành công !", "");
                            common.closeModal();
                            location.reload(true);
                        } else {
                            common.openToastFail("Cập nhật thất bại");
                        }
                    },
                    error: function () {
                        Loading.close();
                    }
                });

        });
    },
};
$(function () {
    serviceType.init();
});




