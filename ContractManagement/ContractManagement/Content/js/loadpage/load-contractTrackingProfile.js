﻿contractTrackingProfile = {
    init: function () {
        let contractTrackingId = $("#contractTrackingId").val();

        common.showLoading();
        $.get("/Home/GetContractTracking", { id: contractTrackingId }, function (response) {
            common.hideLoading();
            $('.modify-contractTracking').click(function () {
                let id = $(this).attr('data-id');
                location.href = "/Home/ModifyContractTracking?id=" + id ;
            });
            document.getElementById('goBackButton').addEventListener('click', function () {
                window.location.href = "/Home/ContractTracking";
            });
        });
    },
    ModifyContractTracking: function (id) {
        common.showLoading();
        $.get("/Home/ModifyContractTracking", { id: id}, function (response) {
            common.hideLoading();
            common.appendModal(response);
            $("#editForm").ajaxForm({
                beforeSubmit: function () { },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", "");
                        common.closeModal();
                        location.reload(true);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });
            $('.btn.btn-white[data-dismiss="modal"]').click(function () {
                window.location.href = "/Home/ContractTracking";
            });
        });
    },

};
$(function () {
    contractTrackingProfile.init();
});



