﻿login = {
    init: function () {
        $("#editForm").ajaxForm(
            {
                beforeSubmit: function () {
                },
                success: function (data) {
                    if (data.Code > 0) {
                        let url = window.location.search + window.location.hash;
                        url = url.replaceAll("?Url=", "");
                        url = decodeURIComponent(url);
                        if (url.length == 0) {
                            url = "/Home/Index";
                        }
                        common.openToastSuccess("Đăng nhập thành công !", url);
                    } else {
                        common.openToastFail("Đăng nhập thất bại !");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });
    },
};
$(function () {
    login.init();
});



