﻿listStaff = {
    init: function () {
        listStaff.GetListStaff();
        listStaff.bindEvents(); // Gọi hàm bindEvents để gán sự kiện cho các phần tử
    },
    GetListStaff: function () {
        $.get("/Home/GetListStaff", function (response) {
            $("#content").html(response); // Chèn file html vào id "#content"
        });
    },
    AddStaff: function () {
        common.showLoading();
        $.get("/Home/AddStaff", function (response) {
            common.hideLoading();
            common.appendModal(response); // Thêm html vào body
            $('#modal').removeAttr('tabindex');
            $(document).ready(function () {
                $('#userId').select2();
            });
            $("#editForm").ajaxForm({
                beforeSubmit: function () { },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", "");
                        common.closeModal();
                        location.reload(true);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });
            $('.btn.btn-white[data-dismiss="modal"]').click(function () {
                window.location.href = "/Home/GetListStaff";
            });
        });
    },

    bindEvents: function () {
        // Lưu giá trị được chọn từ dropdown list "Năm" vào localStorage khi thực hiện tìm kiếm
        $('#form_search').submit(function (event) {
            var selectedYear = $('#selectedYear').val();
            localStorage.setItem('selectedYear', selectedYear);
        });

        // Đặt lại giá trị đã chọn trong dropdown list "Năm" khi trang được tải lại sau khi tìm kiếm
        var storedYear = localStorage.getItem('selectedYear');
        if (storedYear) {
            $('#selectedYear').val(storedYear);
        }

        // Lấy dữ liệu từ localStorage
        var storedDataYear = localStorage.getItem('selectedYear');

        // Gán dữ liệu vào thuộc tính data-year của nút "Sửa"
/*        $('.modify-Kpi').attr('data-year', storedDataYear);*/

        // Sự kiện khi click vào nút "Thêm mới"
        $('.add-staff').click(function () {
            listStaff.AddStaff();
        });
        $('a.btn.btn-primary').click(function () {
            var year = $(this).data('year');
            var url = $(this).attr('href') + '?year=' + year;
            window.location.href = url;
        });

        // Sự kiện khi click vào nút "Sửa"
        $('.view-staff').click(function () {
            let id = $(this).attr("data-id");

            location.href = "/Home/GetStaff?id=" + id;
        });


        // Sự kiện khi click vào nút "Xóa"
        $(".delete-staff").click(function () {
            let id = $(this).attr('data-id');

            if (confirm("Bạn chắc chắn muốn xóa user này?") == true) {
                $.post("/Home/DeleteStaffById", { id: id }, function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !");
                        location.reload(true);
                        listStaff.GetListStaff(year);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                });
            }
        });
    }
};

$(function () {
    listStaff.init();
});
