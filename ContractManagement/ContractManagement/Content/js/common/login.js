﻿login = {
    init: function () {
        var config_site = document.getElementById("config_site");
        var domain = config_site.getAttribute("data-authen-login"); 'https://login-in.vtcnews.vn';// 'https://localhost:7138/';//
        var url_string = window.location.href; //window.location.href
        var Url = new URL(url_string);
        var reUrl = Url.searchParams.get("Url");
        var loginForm = $("#LoginForm");
        var preUrl = "/Account/Login"
        $(".btn-login").click(function () {
            loginForm.attr("src", domain + `?reUrl=${encodeURIComponent(reUrl)}&preUrl=${encodeURIComponent(location.origin + preUrl)}`);
        });

        $(".btn-logout").click(function () {
            var logout_url = config_site.getAttribute("data-authen-logout");
            console.log(logout_url);
            loginForm.attr("src", logout_url);
        });
    },
};
$(function () {
    login.init();
});
