﻿common = {
    init: function () {
        //Cộng chuỗi
        if (!String.prototype.format) {
            String.prototype.format = function () {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function (match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
        if (!String.prototype.toHHMMSS) {
            String.prototype.toHHMMSS = function () {
                var sec_num = parseInt(this, 10); // don't forget the second param
                var hours = Math.floor(sec_num / 3600);
                var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
                var seconds = sec_num - (hours * 3600) - (minutes * 60);
                if (minutes < 10) { minutes = "0" + minutes; }
                if (seconds < 10) { seconds = "0" + seconds; }
                return (hours < 10 ? "" : (hours = "0" + hours + ":")) + minutes + ':' + seconds;
            }
        }
    },
    openToastSuccess: function (msg, url, func) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "1500",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            onHidden: function () {
                if (url != null && url.length > 0)
                    window.location.href = url;
                if (func != null) {
                    func();
                }

            }
        }
        toastr.success(msg, 'Thông báo !')

    },
    openToastFail: function (msg, func) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "4000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            onHidden: function () {
                if (func != null) {
                    func();
                }
            }
        }
        toastr.warning(msg, 'Thông báo !')
    },
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
 
    numberWithCommas: function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    change_alias: function (alias) {
        var str = alias;
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        return str;
    },
    showLoading: function () {
        var main = $("body");
        var load = $('<div class="loading-cms" style="z-index:9999; position: fixed; top: 0; left: 0; right: 0; bottom: 0; background: black; opacity: 0.4; "><img src="/Content/Images/loading.gif" style=" transform: translate(-50%, -50%); position: absolute; left: 50%; top: 30%; width: 100px; "></div>');
        load.appendTo(main);
    },
    hideLoading: function () {
        var main = $("body");

        main.find(".loading-cms").animate({
            "opacity": "0"
        }, 300, function () {
            main.find(".loading-cms").remove();
        });
    },
    closeModal: function () {
        var modal = $("#modal");
        modal.modal('hide');
    },
    appendModal: function (response, urlBack) {
        if ($("#modal").length == 0) {
            var div = document.createElement("div");
            div.innerHTML = '<div class="modal inmodal fade" id="modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true"></div>';
            $('body').append(div.firstChild);
            $("#modal").on('hidden.bs.modal', function () {
                if (urlBack != null && urlBack != "") {
                    location.href = urlBack;
                } else {
                    $("#modal").remove();
                }
            });
        }
        var modal = $("#modal");
        modal.html(response);
        modal.modal('show');
    },

    appendModalImg: function (response, urlBack) {
        if ($("#modal").length == 0) {
            var div = document.createElement("div");
            div.innerHTML = '<div class="modal-img modal inmodal fade" id="modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true"></div>';
            $('body').append(div.firstChild);
            $("#modal").on('hidden.bs.modal', function () {
                if (urlBack != null && urlBack != "") {
                    location.href = urlBack;
                } else {
                    $("#modal").remove();
                }
            });
        }
        var modal = $("#modal");
        modal.html(response);
        modal.modal('show');
    },

    formResult: function (url, sfunc, ffunc) {
        $("#editForm").ajaxForm(
            {
                beforeSubmit: function () {
                },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", url, sfunc);
                    } else {
                        common.openToastFail("Cập nhật thất bại", ffunc);
                    }
                },
                error: function () {
                    Loading.close();
                }
            });
    },
    formUrl: function (url) {
        $("#editForm").ajaxForm(
            {
                beforeSubmit: function () {
                },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !", url);
                    } else {
                        common.openToastFail("Cập nhật thất bại");
                    }
                },
                error: function () {
                    Loading.close();
                }
            });
    },
    formAction: function (sfunc, ffunc) {
        $("#editForm").ajaxForm(
            {
                beforeSubmit: function () {
                },
                success: function (data) {
                    if (data.Code > 0) {
                        common.openToastSuccess("Cập nhật thành công !","",sfunc);
                    } else {
                        common.openToastFail("Cập nhật thất bại", ffunc);
                    }
                },
                error: function () {
                    Loading.close();
                }
            });
    },

    initSearch: function (func) {
        common.showLoading();
        setTimeout(function () {
            common.hideLoading();
            if (func != null) {
                func();
            }
            common.initEventSearch(func);
            window.addEventListener('popstate', function (event) {
                let url = window.location.href;
                common.SearchByUrl(url, func);
            }, false);
        }, 500);
    },
    initEventSearch: function (func) {
        $(".pagination a").click(function (event) {
            event.preventDefault();
            let url = $(this).attr('href');

            //var type = target.getAttribute('data-type');
            window.history.pushState('page2', 'Title', url);
            common.SearchByUrl(url, func);
        });
        $("#form_search").ajaxForm(
            {
                beforeSubmit: function () {
                    common.showLoading();
                    let form_ = $("#form_search");
                    let url = form_.attr("action") + "?" + form_.serialize();
                    let current_url = window.location.pathname + window.location.search;
                    if (url != current_url) {
                        window.history.pushState('page2', 'Title', url);
                    }
                },
                success: function (data) {
                    common.hideLoading();
                    let div = document.createElement("div");
                    div.innerHTML = data;
                    let content_ = div.querySelector("#content");
                    document.querySelector("#content").innerHTML = content_.innerHTML;
                    if (func != null) {
                        func();
                    }
                    common.initEventSearch(func);
                },
                error: function () {
                    common.hideLoading();
                    Loading.close();
                }
            });

    },
    SearchByUrl: function (url, func) {
        common.showLoading();
        $.get(url, function (rs) {
            common.hideLoading();
            let div = document.createElement("div");
            div.innerHTML = rs;
            let content_ = div.querySelector("#content");
            document.querySelector("#content").innerHTML = content_.innerHTML;
            if (func != null) {
                func();
            }
            common.initEventSearch(func);

        });
    },


    getStringDate: function (date) {
        var day = ("0" + date.getDate()).slice(-2);
        var month = ("0" + (date.getMonth() + 1)).slice(-2);
        var str = date.getFullYear() + "-" + (month) + "-" + (day);
        return str;
    },



}

$(function () {
    common.init();
});