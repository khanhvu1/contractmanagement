﻿login = {
    init: function () {

        $("#login_form").validate({
            rules: {
                "Email": {
                    required: true,
                    maxlength: 15
                },
                "PassWord": {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                "Email": {
                    required: "Bắt buộc nhập username",
                    maxlength: "Hãy nhập tối đa 15 ký tự"
                },
                "PassWord": {
                    required: "Bắt buộc nhập password",
                    minlength: "Hãy nhập ít nhất 6 ký tự"
                }
            }
        });
        

        $("#login_form").ajaxForm({
            beforeSubmit: () => {
            },
            success: (response) => {
                if (response.Code > 0) {
                    common.openToastSuccess(response.ResponseMessage, "/");
                } else {
                    common.openToastFail(response.ResponseMessage);
                }
            },
            error: () => {
            }
        });
    }
}

    login.init();
