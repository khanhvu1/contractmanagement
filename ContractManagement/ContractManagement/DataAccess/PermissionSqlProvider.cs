﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using ContractManagement.DataAccess;
using ContractManagement.Libs.Authencation;
namespace ContractManagement.DataAccess
{
    public class PermissionSqlProvider
    {
        static string sqlConnect = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static PermissionSqlProvider Db
        {
            get
            {
                PermissionSqlProvider dataProvider = new PermissionSqlProvider();
                sqlConnect = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                return dataProvider;
            }
        }
        public ReturnInfo Login(LoginModel model)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_Login", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@Email",model.Email),
                        new SqlParameter("@PassWord",model.PassWord)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    var result = DbExtensions.SingleOrDefault<ReturnInfo>(reader);
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }

    }
}