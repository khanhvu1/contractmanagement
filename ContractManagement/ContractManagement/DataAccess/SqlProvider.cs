﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using ContractManagement.Models;
using System.Xml.Linq;
using System.Web.Mvc;
using System.Drawing;

namespace ContractManagement.DataAccess
{
    public class SqlProvider
    {
        static string sqlConnect = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static SqlProvider Db
        {
            get
            {
                SqlProvider dataProvider = new SqlProvider();
                sqlConnect = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                return dataProvider;
            }
        }
        #region Permission
        public int checkAdminPermission(long UserId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_checkPermissionAdmin", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@UserId",UserId),
                        new SqlParameter("@codeAd", SqlDbType.Int) { Direction = ParameterDirection.Output }
                    };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    int codeAd = Convert.ToInt32(sqlComm.Parameters["@codeAd"].Value);
                    return codeAd;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        #endregion
        #region Customers
        public CustomerModel GetListCustomer(CustomerModel model)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetListCustomer", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@Key", model.Key),
                        new SqlParameter("@PageIndex", model.CurrentPage),
                        new SqlParameter("@PageSize", model.PageSize),
                        new SqlParameter("@TotalRecord", SqlDbType.Int) { Direction = ParameterDirection.Output }
                };
                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();
                if (reader != null)
                {
                    model.Result = DbExtensions.ToList<Customers>(reader);
                }
                if (sqlcomm.Parameters["@TotalRecord"].Value != null)
                {
                    model.TotalRecord = Convert.ToInt32(sqlcomm.Parameters["@TotalRecord"].Value);
                }

                return model;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                model.Result = new List<Customers>();
                return model;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public Customers GetCustomerById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            Customers result = new Customers();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetCustomerInfoByCustomerId", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
		                  //Tham số truyền vào
		                  new SqlParameter("@CustomerID",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<Customers>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new Customers();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyCustomer(Customer model, long ControllerId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_ModifyCustomerInfo", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    // Thiết lập tên của stored procedure trong CommandText
                    sqlComm.CommandText = "cms_ModifyCustomerInfo";

                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                new SqlParameter("@ControllerId", ControllerId),
                new SqlParameter("@CustomerId", model.CustomerID),
                new SqlParameter("@CustomerName", model.CustomerName),
                new SqlParameter("@Address", model.Address),
                new SqlParameter("@TaxCode", model.TaxCode),
                new SqlParameter("@Phone", model.Phone),
                new SqlParameter("@Email", model.Email),
                new SqlParameter("@FoundationDate", model.FoundationDate),
                new SqlParameter("@IndustryID", model.IndustryID),
                new SqlParameter("@NewCustomerID", SqlDbType.Int) { Direction = ParameterDirection.Output }
            };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    // Trả về giá trị của NewCustomerID từ stored procedure
                    //int newId = Convert.ToInt32(sqlComm.Parameters["@NewCustomerID"].Value);
                    object newIdValue = sqlComm.Parameters["@NewCustomerID"].Value;
                    int newId = (newIdValue != DBNull.Value && newIdValue != null) ? Convert.ToInt32(newIdValue) : model.CustomerID ?? 0;
                    return newId;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }

        public int DeleteCustomerById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteCustomerInfoByCustomerId", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@CustomerID",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<Industry> SelectIndustry()
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<Industry> result = new List<Industry>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetListIndustry", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<Industry>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<Industry>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<Customers> ExportListCustomer()
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<Customers> result = new List<Customers>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_ExportListCustomer", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<Customers>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<Customers>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }

        #endregion
        #region ContactPerson
        public ContactPerson GetContactPersonById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            ContactPerson result = new ContactPerson();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetContactPersonById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
		                  //Tham số truyền vào
		                  new SqlParameter("@PersonID",id)
                      };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<ContactPerson>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new ContactPerson();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<ContactPerson> GetListContactPersonByCustomerId(int customerId)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<ContactPerson> result = new List<ContactPerson>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("[cms_GetListContactPersonByCustomerId]", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                          new SqlParameter("@CustomerID",customerId),
                      };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<ContactPerson>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<ContactPerson>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyContactPerson(ContactPerson model, long ControllerId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_ModifyContactPersons", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                            new SqlParameter("@ControllerId",ControllerId),
                            new SqlParameter("@PersonID",model.PersonID),
                            new SqlParameter("@CustomerID",model.CustomerID),
                            new SqlParameter("@FullName",model.FullName),
                            new SqlParameter("@PhoneNumber",model.PhoneNumber),
                            new SqlParameter("@EmailContact",model.EmailContact),
                            new SqlParameter("@Address",model.Address),
                            new SqlParameter("@BirthDay",model.BirthDay),
                          };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int DeleteContactPersonById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteContactPersonByPersonID", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                          new SqlParameter("@PersonID",id)
                      };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        #endregion
        #region RevenueDebt
        public SearchRevenueDebt GetRevenueDebt(SearchRevenueDebt model)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetRevenueDebt", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@Key", model.Key),
                        new SqlParameter("@Year", model.Year),
                        new SqlParameter("@PageIndex", model.CurrentPage),
                        new SqlParameter("@PageSize", model.PageSize),
                        new SqlParameter("@TotalRecord", SqlDbType.Int) { Direction = ParameterDirection.Output }
                };
                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();
                if (reader != null)
                {
                    model.Result = DbExtensions.ToList<RevenueDebtModel>(reader);
                }
                if (sqlcomm.Parameters["@TotalRecord"].Value != null)
                {
                    model.TotalRecord = Convert.ToInt32(sqlcomm.Parameters["@TotalRecord"].Value);
                }

                return model;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                model.Result = new List<RevenueDebtModel>();
                return model;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public RevenueDebtModel GetContractById(int id,int year)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            RevenueDebtModel result = new RevenueDebtModel();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetContractById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
		                  //Tham số truyền vào
		                  new SqlParameter("@RevenueDebtId",id),
		                  new SqlParameter("@Year",year)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<RevenueDebtModel>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new RevenueDebtModel();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyContract(RevenueDebt model, long ControllerId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_ModifyContract", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    // Thiết lập tên của stored procedure trong CommandText
                    sqlComm.CommandText = "cms_ModifyContract";

                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@ControllerId",ControllerId),
                        new SqlParameter("@RevenueDebtId",model.RevenueDebtId),
                        new SqlParameter("@CustomerID",model.CustomerID),
                        new SqlParameter("@ContractNumber",model.ContractNumber),
                        new SqlParameter("@SigningDate",model.SigningDate),
                        new SqlParameter("@ExpiryDate",model.ExpiryDate),
                        new SqlParameter("@ContractValue",model.ContractValue),
                        new SqlParameter("@UserId",model.UserId),
                        new SqlParameter("@Year",model.Year),
                        new SqlParameter("@Revenue1",model.Revenue1),
                        new SqlParameter("@Revenue2",model.Revenue2),
                        new SqlParameter("@Revenue3",model.Revenue3),
                        new SqlParameter("@Revenue4",model.Revenue4),
                        new SqlParameter("@Revenue5",model.Revenue5),
                        new SqlParameter("@Revenue6",model.Revenue6),
                        new SqlParameter("@Revenue7",model.Revenue7),
                        new SqlParameter("@Revenue8",model.Revenue8),
                        new SqlParameter("@Revenue9",model.Revenue9),
                        new SqlParameter("@Revenue10",model.Revenue10),
                        new SqlParameter("@Revenue11",model.Revenue11),
                        new SqlParameter("@Revenue12",model.Revenue12),
                        new SqlParameter("@NewRevenueDebtId", SqlDbType.Int) { Direction = ParameterDirection.Output }
            };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();
                    object newIdValue = sqlComm.Parameters["@NewRevenueDebtId"].Value;
                    int newId = (newIdValue != DBNull.Value && newIdValue != null) ? Convert.ToInt32(newIdValue) : model.RevenueDebtId ?? 0;
                    return newId;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int DeleteContractById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteContractById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Id",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<Customer> SelectCustomer()
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<Customer> result = new List<Customer>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_SelectCustomer", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<Customer>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<Customer>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<ListStaff> Selectpersonnel()
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<ListStaff> result = new List<ListStaff>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_Selectpersonnel", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<ListStaff>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<ListStaff>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public DataSet ExportRevenueDebt( int year)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            var ds = new DataSet();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_ExportRevenueDebt", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                new SqlParameter("@Year", year),
        };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlcomm);
                sqlDataAdapter.Fill(ds);
                //ds = await SqlAsynProvider.GetDataSetAsync(sqlcomm);
                return ds;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return new DataSet();
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        #endregion
        #region Staff
        public SearchStaff GetListStaff(SearchStaff model)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetListStaff", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@Key", model.Key),
                        new SqlParameter("@Year", model.Year),
                        new SqlParameter("@PageIndex", model.CurrentPage),
                        new SqlParameter("@PageSize", model.PageSize),
                        new SqlParameter("@TotalRecord", SqlDbType.Int) { Direction = ParameterDirection.Output }
                };
                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();
                if (reader != null)
                {
                    model.Result = DbExtensions.ToList<ListStaffModel>(reader);
                }
                if (sqlcomm.Parameters["@TotalRecord"].Value != null)
                {
                    model.TotalRecord = Convert.ToInt32(sqlcomm.Parameters["@TotalRecord"].Value);
                }

                return model;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                model.Result = new List<ListStaffModel>();
                return model;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public ListKpi GetStaff(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            ListKpi result = new ListKpi();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("[cms_GetStaffById]", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
		                  //Tham số truyền vào
		                  new SqlParameter("@Id",id),

                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<ListKpi>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new ListKpi();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int AddStaff(ListStaff model, long ControllerId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_AddStaff", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                            new SqlParameter("@ControllerId",ControllerId),
                            new SqlParameter("@UserId",model.UserId),
                            new SqlParameter("@UserName",model.UserName),
                            new SqlParameter("@Id",model.Id),
                          };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int ModifyKpi(KPI model, long ControllerId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_ModifyKpi", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                            new SqlParameter("@ControllerId",ControllerId),
                            new SqlParameter("@UserId",model.UserId),
                            new SqlParameter("@Kpi1",model.Kpi1),
                            new SqlParameter("@Kpi2",model.Kpi2),
                            new SqlParameter("@Kpi3",model.Kpi3),
                            new SqlParameter("@Kpi4",model.Kpi4),
                            new SqlParameter("@Kpi5",model.Kpi5),
                            new SqlParameter("@Kpi6",model.Kpi6),
                            new SqlParameter("@Kpi7",model.Kpi7),
                            new SqlParameter("@Kpi8",model.Kpi8),
                            new SqlParameter("@Kpi9",model.Kpi9),
                            new SqlParameter("@Kpi10",model.Kpi10),
                            new SqlParameter("@Kpi11",model.Kpi11),
                            new SqlParameter("@Kpi12",model.Kpi12),
                            new SqlParameter("@Year",model.Year),
                            new SqlParameter("@Id",model.Id),
                          };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int DeleteStaffById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteStaffById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Id",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<StaffModel> SelectStaff()
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<StaffModel> result = new List<StaffModel>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_SelectStaff", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<StaffModel>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<StaffModel>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<ListStaffModel> ExportListStaff(int year)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<ListStaffModel> result = new List<ListStaffModel>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_ExportListStaff", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Year",year)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<ListStaffModel>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<ListStaffModel>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }

        #endregion
        #region Revenue
        public RevenueModel GetRevenueById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            RevenueModel result = new RevenueModel();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetRevenueById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    //Tham số truyền vào
                    new SqlParameter("@RevenueId",id),

                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<RevenueModel>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new RevenueModel();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<RevenueModel> GetListRevenueByRevenueDebtId(int revenueDebtId)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<RevenueModel> result = new List<RevenueModel>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("[cms_GetListRevenueByRevenueDebtId]", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@RevenueDebtId",revenueDebtId),
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<RevenueModel>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<RevenueModel>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyRevenue(Revenue model, long ControllerId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_ModifyRevenue", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@RevenueId",model.RevenueId),
                        new SqlParameter("@RevenueDebtId",model.RevenueDebtId),
                        new SqlParameter("@TypeId",model.TypeId),
                        new SqlParameter("@RevenueValue",model.RevenueValue),
                        new SqlParameter("@ControllerId",ControllerId),
                    };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int DeleteRevenueById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteRevenueById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@RevenueId",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<RevenueType> SelectTypeRevenue()
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<RevenueType> result = new List<RevenueType>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_SelectTypeRevenue", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<RevenueType>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<RevenueType>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        #endregion
        #region ContractTracking
        public SearchContractTracking ContractTracking(SearchContractTracking model)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            try
            {
                SqlCommand sqlcomm = new SqlCommand("[cms_GetListContractTracking]", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@Key", model.Key),
                        new SqlParameter("@Year", model.Year),
                        new SqlParameter("@PageIndex", model.CurrentPage),
                        new SqlParameter("@PageSize", model.PageSize),
                        new SqlParameter("@TotalRecord", SqlDbType.Int) { Direction = ParameterDirection.Output }
                };
                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();
                if (reader != null)
                {
                    model.Result = DbExtensions.ToList<ContrackTrackingModel>(reader);
                }
                if (sqlcomm.Parameters["@TotalRecord"].Value != null)
                {
                    model.TotalRecord = Convert.ToInt32(sqlcomm.Parameters["@TotalRecord"].Value);
                }

                return model;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                model.Result = new List<ContrackTrackingModel>();
                return model;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyContractTracking(ContractTracking model, long ControllerId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("[cms_ModifyContractTracking]", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                            new SqlParameter("@ControllerId",ControllerId),
                            new SqlParameter("@Note",model.Note),
                            new SqlParameter("@ContractTrackingId",model.ContractTrackingId),
                          };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public ContrackTrackingModel GetContractTrackingById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            ContrackTrackingModel result = new ContrackTrackingModel();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("[cms_GetContractTrackingById]", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
		                  //Tham số truyền vào
		                  new SqlParameter("@Id",id),
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<ContrackTrackingModel>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new ContrackTrackingModel();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public DataSet ExportContractTracking(int year)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            var ds = new DataSet();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("[cms_ExportContractTracking]", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                new SqlParameter("@Year", year),
        };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlcomm);
                sqlDataAdapter.Fill(ds);
                //ds = await SqlAsynProvider.GetDataSetAsync(sqlcomm);
                return ds;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return new DataSet();
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        #endregion
        #region Service
        public ServiceModel GetServiceById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            ServiceModel result = new ServiceModel();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetServiceById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    //Tham số truyền vào
                    new SqlParameter("@ServiceId",id),

                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<ServiceModel>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new ServiceModel();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<ServiceModel> GetListServiceByContractTrackingId(int contractTrackingId)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<ServiceModel> result = new List<ServiceModel>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("[cms_GetListServiceByContrackTrackingId]", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@ContractTrackingId",contractTrackingId),
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<ServiceModel>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<ServiceModel>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyService(Service model)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("[cms_ModifyService]", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@ServiceId",model.ServiceId),
                        new SqlParameter("@TypeId",model.TypeId),
                        new SqlParameter("@ContractTrackingId",model.ContractTrackingId),
                        new SqlParameter("@ServiceStatus",model.ServiceStatus),
                    };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int DeleteServiceById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteServiceById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@ServiceId",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<SubscriptionService> SelectTypeService()
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<SubscriptionService> result = new List<SubscriptionService>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("[cms_SelectTypeService]", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<SubscriptionService>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<SubscriptionService>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        #endregion
        #region Debt
        public Debt GetDebtById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            Debt result = new Debt();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetDebtById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    //Tham số truyền vào
                    new SqlParameter("@DebtID",id),

                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<Debt>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new Debt();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public List<Debt> GetListDebtByRevenueDebtId(int revenueDebtId)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            List<Debt> result = new List<Debt>();

            try
            {
                SqlCommand sqlcomm = new SqlCommand("[cms_GetListDebtByRevenueDebtId]", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@RevenueDebtId",revenueDebtId),
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.ToList<Debt>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new List<Debt>();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyDebt(Debt model, long ControllerId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_ModifyDebt", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@DebtID",model.DebtID),
                        new SqlParameter("@RevenueDebtId",model.RevenueDebtId),
                        new SqlParameter("@DebtCollected",model.DebtCollected),
                        new SqlParameter("@Time",model.Time),
                        new SqlParameter("@ControllerId",ControllerId),
                    };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int DeleteDebtById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteDebtById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@DebtId",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        #endregion
        #region OldDebt
        public OldDebt GetOldDebtById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            OldDebt result = new OldDebt();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetOldDebtById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    //Tham số truyền vào
                    new SqlParameter("@OldDebtID",id),

                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<OldDebt>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new OldDebt();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public SearchOldDebt GetListOldDebt(SearchOldDebt model)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetListOldDebt", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@Key", model.Key),
                        new SqlParameter("@PageIndex", model.CurrentPage),
                        new SqlParameter("@PageSize", model.PageSize),
                        new SqlParameter("@TotalRecord", SqlDbType.Int) { Direction = ParameterDirection.Output }
                };
                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();
                if (reader != null)
                {
                    model.Result = DbExtensions.ToList<OldDebtModel>(reader);
                }
                if (sqlcomm.Parameters["@TotalRecord"].Value != null)
                {
                    model.TotalRecord = Convert.ToInt32(sqlcomm.Parameters["@TotalRecord"].Value);
                }

                return model;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                model.Result = new List<OldDebtModel>();
                return model;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyOldDebt(OldDebt model, long ControllerId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_ModifyOldDebt", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@OldDebtID",model.OldDebtID),
                        new SqlParameter("@CustomerID",model.CustomerID),
                        new SqlParameter("@OldDebtValue",model.OldDebtValue),
                        new SqlParameter("@Year",model.Year),
                        new SqlParameter("@ControllerId",ControllerId),
                    };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int DeleteOldDebtById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteOldDebtById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@OldDebtId",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        #endregion
        #region RevenueType
        public RevenueType GetRevenueTypeById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            RevenueType result = new RevenueType();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetRevenueTypeById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    //Tham số truyền vào
                    new SqlParameter("@Id",id),

                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<RevenueType>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new RevenueType();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyRevenueType(RevenueType model, long UserId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_ModifyRevenueType", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@Id",model.Id),
                        new SqlParameter("@Name",model.Name),
                        new SqlParameter("@UserId",UserId),
                    };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int DeleteRevenueTypeById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteRevenueTypeById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Id",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }

        #endregion
        #region ServiceType
        public SubscriptionService GetServiceTypeById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);
            SubscriptionService result = new SubscriptionService();
            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_GetServiceTypeById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    //Tham số truyền vào
                    new SqlParameter("@Id",id),

                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                IDataReader reader = sqlcomm.ExecuteReader();

                if (reader != null)
                {
                    result = DbExtensions.SingleOrDefault<SubscriptionService>(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                result = new SubscriptionService();
                return result;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        public int ModifyServiceType(SubscriptionService model, long UserId)
        {
            using (SqlConnection sqlConn = new SqlConnection(sqlConnect))
            {
                try
                {
                    SqlCommand sqlComm = new SqlCommand("cms_ModifyServiceType", sqlConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@Id",model.Id),
                        new SqlParameter("@Name",model.Name),
                        new SqlParameter("@Description",model.Description),
                        new SqlParameter("@UserId",UserId),
                    };

                    sqlComm.Parameters.AddRange(parameters);
                    sqlComm.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return -1;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                }
            }
        }
        public int DeleteServiceTypeById(int id)
        {
            SqlConnection cnn = new SqlConnection(sqlConnect);


            try
            {
                SqlCommand sqlcomm = new SqlCommand("cms_DeleteServiceTypeById", cnn);
                cnn.Open();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Id",id)
                };

                sqlcomm.Parameters.AddRange(sqlParameters);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                return -1;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }

        #endregion

    }
}