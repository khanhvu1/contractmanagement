﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Libs
{
    public class Helpers
    {
        public static string BuildPagging(int totalPage, int pageIndex, string url)
        {
            string build = "";
            if (pageIndex > 1)
            {
                build += "<li class=\"page-item\"><a href=\"" + url + "&CurrentPage=" + (pageIndex - 1).ToString() + "\" class=\"page-link\">Pre</a></li>";
                build += "<li class=\"page-item\"><a href=\"" + url + "&CurrentPage=" + (pageIndex - 1).ToString() + "\" class=\"page-link\">" + (pageIndex - 1).ToString() + "</a></li>";
            }
            build += "<li class=\"page-item active\"><span class=\"page-link\">" + (pageIndex).ToString() + "</span></li>";

            if (pageIndex + 1 <= totalPage)
            {
                build += "<li class=\"page-item\"><a href=\"" + url + "&CurrentPage=" + (pageIndex + 1).ToString() + "\" class=\"page-link\">" + (pageIndex + 1).ToString() + "</a></li>";
                build += "<li class=\"page-item\"><a href=\"" + url + "&CurrentPage=" + (pageIndex + 1).ToString() + "\" class=\"page-link\">Next</a></li>";
            }
            build = "<ul class=\"pagination\">" + build + "</ul>";
            return build;
        }
    }
}