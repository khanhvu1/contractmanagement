﻿using ContractManagement.Libs.Authencation.Document.Authencation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Libs.Authencation
{
    public static class Constains
    {
        public const string CURRENT_USER_ID = "CURRENT_USER_ID";
        public const string CURRENT_USER = "CURRENT_USER";
        public const string CurrentLanguage = "CURRENTLANGUAGE";
        public const string TIME_SAVE_DRAFF = "TIME_SAVE_DRAFF";
        public const string SESSION_USER_ID = "SESSION_USER_ID";
        public const string SESSION_USER_SID = "SESSION_USER_SID";
        public const string SESSION_USERNAME = "SESSION_USERNAME";
        public const string SESSION_USER_FULLNAME = "SESSION_USER_FULLNAME";
        public const string SESSION_USER_EMAIL = "SESSION_USER_EMAIL";
        public const string SESSION_USER_PERMISSION = "SESSION_USER_PERMISSION";
        public const string SESSION_DRAFF = "SESSION_DRAFF";
        public const string SESSION_USER_MOBILE = "SESSION_USER_MOBILE";
        public const int SESSION_EXPIRES = 60;

        public const string SESSION_USER_AVATAR = "SESSION_USER_AVATAR";
        public const string SESSION_USER_CREATEDDATE = "SESSION_USER_CREATEDDATE";
        public const string SESSION_USER_GROUP = "SESSION_USER_GROUP";

        public const string SESSION_USER_USE_MOBILE = "SESSION_USER_USE_MOBILE";

        public const string AVATAR_DEFAULT = "/Content/pc/images/no-avatar.png";
        public const string AVATAR_DEFAULT_CMS = "/Content/Images/no-avatar.jpg";
        public const string SESSION_PERMISSION = "SESSION_PERMISSION";

        public static string VERSION_CACHE = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["VERSION_CACHE"]);

        public static string DELIVERY_SERVER = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DELIVERY_SERVER"]);

        //CMS Config
        public static int NUMBER_LIMIT_TITLE = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NUMBER_LIMIT_TITLE"]);
        public static int NUMBER_LIMIT_DESCRIPTION = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NUMBER_LIMIT_DESCRIPTION"]);

        public static int LIMIT_TIMES_TOKEN = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["LIMIT_TIMES_TOKEN"]);
        public static int SECOND_EXPIRE_TOKEN = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SECOND_EXPIRE_TOKEN"]);


    }
    public class LoginModel
    {
        public string Email { get; set; }
        public string PassWord { get; set; }
    }
    public class ReturnInfo
    {
        public int Id { get; set; }
        public string Email { get; set; }
    }
    public class SystemBL
    {
        static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static void Login(int Id, string Email)
        {
            AccountUtil.Id = Id;
            AccountUtil.Email = Email;
        }
    }
}