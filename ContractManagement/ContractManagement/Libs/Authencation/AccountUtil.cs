﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace ContractManagement.Libs.Authencation
{
    namespace Document.Authencation
    {
        public class AccountUtil
        {
            public static bool IsLogin
            {
                get
                {
                    return !((Id == -1) || (UserName == string.Empty));
                }
            }

            /// <summary>
            /// Mã người dùng
            /// </summary>
            public static int Id
            {
                get
                {
                    try
                    {
                        return Convert.ToInt32(SessionHelper.GetSession(Constains.SESSION_USER_ID));
                    }
                    catch { return -1; }

                }
                set
                {
                    SessionHelper.SetSession(Constains.SESSION_USER_ID, value.ToString());
                }
            }

            public static int StaffId
            {
                get
                {
                    try
                    {
                        return Convert.ToInt32(SessionHelper.GetSession(Constains.SESSION_USER_SID));
                    }
                    catch { return -1; }

                }
                set
                {
                    SessionHelper.SetSession(Constains.SESSION_USER_SID, value.ToString());
                }
            }

            public static string UserName
            {
                get
                {
                    var debugmode = Convert.ToInt16(ConfigurationManager.AppSettings["DEBUG_MODE"]);
                    if (debugmode == 1)
                        return ConfigurationManager.AppSettings["DEBUG_ACCOUNTNAME"];

                    return SessionHelper.GetSession(Constains.SESSION_USERNAME);
                }
                set
                {
                    SessionHelper.SetSession(Constains.SESSION_USERNAME, value);
                }
            }


            public static string Email
            {
                get
                {
                    return SessionHelper.GetSession(Constains.SESSION_USER_EMAIL);
                }
                set
                {
                    SessionHelper.SetSession(Constains.SESSION_USER_EMAIL, value);
                }
            }

            public static string Avatar
            {
                get
                {
                    string avt = SessionHelper.GetSession(Constains.SESSION_USER_AVATAR);
                    if (avt == null || avt == string.Empty)
                        avt = Constains.AVATAR_DEFAULT;
                    return avt;
                }
                set
                {
                    SessionHelper.SetSession(Constains.SESSION_USER_AVATAR, value);
                }
            }
        }
    }
}