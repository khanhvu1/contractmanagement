﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractManagement.Libs.Authencation
{
    public class SessionHelper
    {
        public static string GetSession(string name)
        {
            try
            {
                var session = System.Web.HttpContext.Current.Session[name];
                if (session != null)
                    return session.ToString();
                else
                    return string.Empty;
            }
            catch
            {
                return string.Empty;

            }
        }

        public static void SetSession(string name, string value)
        {
            int sessionExpire = 60;
            System.Web.HttpContext.Current.Session.Timeout = sessionExpire;
            System.Web.HttpContext.Current.Session[name] = value;
        }
    }
}