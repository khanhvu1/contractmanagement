﻿using ContractManagement.DataAccess;
using ContractManagement.Libs.Authencation.Document.Authencation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractManagement.Libs.Authencation
{
    public class FilterPermission : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            filterContext.Controller.ViewBag.Name = AccountUtil.UserName;
            var debugMode = Convert.ToBoolean(ConfigurationManager.AppSettings["DEBUGMODE"]);
            if (debugMode)
            {
                int Id = Convert.ToInt32(ConfigurationManager.AppSettings["DEBUG_USER_ID"]);
                string Email = ConfigurationManager.AppSettings["DEBUG_USER_EMAIL"];
                SystemBL.Login(Id, Email);
                if (!filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Controller.ViewBag.Id = AccountUtil.Id;
                    filterContext.Controller.ViewBag.FullName = AccountUtil.Email;
                }
                //var code = SqlProvider.Db.checkPermission(AccountUtil.Id);
                //filterContext.Controller.ViewBag.Permission = code;

            }
            else
            {
                var checkSkipAuthorzation = SkipAuthorization(filterContext);
                if (!AccountUtil.IsLogin && !checkSkipAuthorzation)
                {
                    var returnUrl = filterContext.HttpContext.Request.Url.PathAndQuery;
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        var urlHelper = new UrlHelper(filterContext.RequestContext);
                        filterContext.HttpContext.Response.StatusCode = 403;

                        filterContext.Result = new JsonResult
                        {
                            Data = new
                            {
                                Error = "NotAuthorized",
                                LogOnUrl = urlHelper.Action("Login", "Home", new { Url = HttpContext.Current.Server.UrlEncode(returnUrl) })
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                        filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
                        filterContext.HttpContext.Response.ContentType = "application/json; charset=utf-8";

                    }
                    else
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/Login?Url=" + HttpContext.Current.Server.UrlEncode(returnUrl));
                    }
                }



                if (!filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Controller.ViewBag.Id = AccountUtil.Id;
                    filterContext.Controller.ViewBag.FullName = AccountUtil.Email;
                }
            }
        }
        private static bool SkipAuthorization(AuthorizationContext filterContext)
        {
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
                                                     || filterContext.ActionDescriptor.
                                                      ControllerDescriptor.
                                                     IsDefined(typeof(AllowAnonymousAttribute), true);

            return skipAuthorization;
        }

    }
}